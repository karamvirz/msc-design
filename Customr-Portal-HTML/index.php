<?php include('header.php')?>	
	<div class="container main-container account-pages">
	<div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">My Credit</h4>
				<ul class="nav side-nav"> 
				<li><a href="index.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li class="active"> <a href="my-credit/credit-factors.php"><i class="ion-clipboard"></i> Credit Factors</a> </li>
				 <li> <a href="my-credit/credit-report.php"><i class="ion-document-text"></i> Credit Report</a></li>
				 <li><a href="my-credit/dispute-progress.php"><i class="ion-arrow-graph-up-right"></i>Dispute Progress</a></li> 
				 <li><a href="my-credit/upload-documents.php"><i class="ion-upload"></i> Upload Documents</a> </li> 
				 </ul> 
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>My Credit Score</h3>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  score-meter STARTS here ========--->
		
		<section class="score-meter">
		
		<div class="meter col-md-9">
		<div class="provided-by">
		<span class="col-md-5">Provided by: </span> 
		<img src="images/transu-large.png">
		</div>
		<div class="clearfix"></div>
		 <div class="score-count">760</div>
		 <div class="score-rating">Credit Rating : <strong>Excellent</strong></div>
		 <div class="progress-container">
		  <div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style="width: 82%;"></div>
		  <div class="clearfix"></div>
		 <div class="progress meter-progress">
		 <span class="poor"></span>
		 <span class="fair"></span>
		 <span class="average"></span>
		 <span class="good"></span>
		 <span class="excellent"></span>
		 </div>
		  <div class="clearfix"></div>
		 <ul class="nav progress-counting">
		 <li>300</li>
		 <li>580</li>
		 <li>640</li>
		 <li>700</li>
		 <li>750</li>
		 <li>850</li>
		  </ul>
          </div><!---- progress-container-->   
		<p class="updation text-center"><i class="ion-clock"></i> Updated June 21,2016 &nbsp;|&nbsp; Next Update in 6 days</p>
		
		</div><!---- meter Close here-->

		<div class="meter-des col-md-3">
		 <p><span class="excellent"></span> Excellent 750+</p>
		 <p><span class="good"></span>Good 700-749</p>
		 <p><span class="average"></span>Average 660-699</p>
		 <p><span class="fair"></span>Poor 580-659</p>
	     <p> <span class="poor"></span>Bad &lt; 580</p>
		</div>
		
		<div class="clearfix"></div>
		<center>
		<a class="btn btn-primary btn-lg">View Entire Report</a>
        </center>
		</section>
		<!----============  score-meter END here ===========--->
		
		<div class="clearfix"></div>
		
		<div class="section-title">
		<h3>Dispute Progress</h3>
		</div>
		<div class="clearfix"></div>
		
		
		<!----==============  dispute-progress START ========--->
		<section class="dispute-progress padd_left_0 ">
          <div class="overlay">
          <div class="dispute-message text-center">
		  <h3>Start Repairing Your Credit</h3>		   
		   <center>
		   <a class="btn btn-primary btn-xl" data-toggle="modal" data-target=".dispute-light-box">
		   Get Started</a></center>
		     </div>          
		     </div>  

       <div class="dispute-selection">			 
		  
		<div class="col-md-4 nagative-pie-container ">
		  
		  <div class="nagative-pie">
        <h3 class="subtitle text-center"> Negative Removed</h3>			  
		 <div class="c100 p32 medium blue">
                    <span>33 <span class="small">%</span> <small>Complete</small></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>		
	      </div>
		  <div class="short-detail-nagatives">
		  <a href="#" class="removed">5 Removed</a> |  <a href="#" class="remaining"> 10 Remaining</a> 
		   </div>
         <div class="clearfix"></div>
             <center> 
		     <a class="btn btn-primary btn-md">Details of all Negative Items</a>
			 </center>
		    </div>
			
			<!---=== nagative-pie-container END ====-->			
		    <div class="col-md-8 nagatives-bargraph padd_right_0">
			   <h3 class="subtitle text-center"> Numbers of Negatives items</h3>		   
			   <div id="graph"></div>
				</div>
			<!---===== nagatives-bargraph END =====--->
			
		</div>
		</section>
<!----===============  dispute-progress END =======--->
		
			
<!----===============  Disputed Table START =======--->
		<div class="clearfix"></div>
		
		<div class="section-title"><h3>What’s Being Disputed</h3></div>
		
		<section class="Disputed-data">
		<div class="table-responsive">
		<table class="table table-bordered">
		    <tr>
			<th>&nbsp;</td>
			<th><img src="images/transu-s.png"></th>
			<th><img src="images/experian-s.png"></th>
			<th><img src="images/equifax-s.png"></th>		   
		   </tr>
		   
		    <tr>
			<td>Last Round Date</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		    <tr>
			<td>Bureau response due date</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		   
		    <tr>
			<td> # of items in dispute this round</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		  <tr>
			<td>Total items disputed</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		
			</table>
			</div>
         </section>
		 <div class="clearfix"></div>
<!----===============  Disputed Table END =======--->

<!----===============  Coordinator  Detail Start=======--->
		
		<div class="section-title"><h3>Customer Support</h3></div>
         <section class="Contacts"> 
		 
		 <div class=" Coordinator-desc">
		 <div class="col-md-6 col-sm-6"><i class="ion-iphone"></i> 123 -456-7890</div>
		 <div class="col-md-6 col-sm-6"><i class="ion-email"></i> Email@email.com</div>
		 </div>
		  </section>
<!--==== Coordinator END =====--->
		

		</div>
<!----===========  content-container END here ======--->	

</div><!----  Row END here --->
 </div><!----  Container END here --->
 
 <!---------- Light Box ----------->
 
 
 <div class="modal fade dispute-light-box" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
	<div class="modal-header"> 
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	<i class="ion-ios-close-empty" aria-hidden="true"></i></button> 
	<h4 class="modal-title text-center" id="myLargeModalLabel">What Will Be Disputed</h4> </div>	
	<div class="modal-body">
     <form class="form-horizontal">	
	 <div class="dispute-short-detail">
        <div class="col-md-3 col-sm-6 col-xs-6 text-center total-accounts">
		  <h1>22</h1>
		   <h5>Accounts</h5>
		   </div>	
		   
        <div class="col-md-4 col-sm-6 col-xs-6 total-nagatives text-center">
		 <h1>15</h1>
		   <h5>Negatives</h5>
		  </div>

         <div class="dispute-short-detail-divider  clearfix"></div>	  
		  
		   <div class="col-md-5 col-sm-12">
		   <a class="btn btn-lg btn-block btn-primary">Start Disputing Now</a>
			   <div class="checkbox fancy_checkbox fancy_checkbox-sm">
				<label>
				  <input type="checkbox"> <span>I agree to dispute all checked items</span>
				</label>
			  </div>	   
		    </div>
		  
		  
	   </div>
	   <!--- dispute-short-detail END-->
	   
	    <!--- dispute-full-detail Start-->
	   
	     <div class="dispute-full-detail-des text-center">
	       <h4>Details</h4>
		   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
	      </div>
	      
		  <div class="clearfix"></div>
		  
		  
		  <div class="table-responsive">
	   
	   <table class="table nagative-details-table table-hover">
				 <thead>
					 <tr>
					 <th>
					 <div class="checkbox fancy_checkbox">
						<label><input type="checkbox"><span></span></label>
			           </div>
					   </th>
					 <th>Account Name</th>
					 <th> Account Number</th>
					 <th>Account Type</th>
					 <th>Negative Type</th>
					 <th>Select Reason</th>
				 </tr></thead>
				 <tbody>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td><select class="form-control fancy-select input-sm">
       					  <option>Dispute Reason</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>						 
						 </select></td>						
						
					 </tr>
					 
					  <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td><select class="form-control fancy-select input-sm">
       					  <option>Dispute Reason</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>						 
						 </select></td>						
						
					 </tr>
					 
					  <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td><select class="form-control fancy-select input-sm">
       					  <option>Dispute Reason</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>						 
						 </select></td>						
						
					 </tr>
					 
					  <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td><select class="form-control fancy-select input-sm">
       					  <option>Dispute Reason</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>						 
						 </select></td>						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td><select class="form-control fancy-select input-sm">
       					  <option>Dispute Reason</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>						 
						 </select></td>						
						
					 </tr>

					 </tbody> 
					 </table> 
					   </div>
					 
				<div class="clearfix"></div>

         <nav class="pagination-nav"> 	
        <div class="col-md-3  padd_0">
		  <label class="col-md-6 col-xs-6 padd_0 show-rows">Show rows:</label>
		  <div class="col-md-4 col-xs-6 padd_0">
			  <select class="form-control fancy-select input-sm">
							  <option>5</option>
							  <option>10</option>
							  <option>25</option>
							  <option>50</option>						 
							  <option>100</option>						 
							 </select>
		     </div>	   
		    </div>		 
			 <ul class="pagination pull-right"> 
				 <li class="disabled"><a aria-label="Previous" href="#">
				 <i class="ion-ios-arrow-thin-left"></i></a></li> 
				 <li class="active"><a href="#">1</a></li> 
				 <li><a href="#">2</a></li> 
				 <li><a href="#">3</a></li> 
				 <li><a href="#">4</a></li> 
				 <li><a href="#">5</a></li>
				 <li><a aria-label="Next" href="#"><i class="ion-ios-arrow-thin-right"></i></a></li>
			 </ul> </nav>				
	   
	    <div class="clearfix"></div><br><br>
		
		
   <div class="dispute-light-bottom">		
    <div class="checkbox fancy_checkbox col-md-5 padd_0 fancy_checkbox-sm">
				<label>
				  <input type="checkbox"> <span>I agree to dispute all checked items</span>
				</label>
			  </div>
            <div class="col-md-3 padd_0"><a class="btn btn-md btn-primary btn-block">Start Disputing</a></div>
             </div>			
	      
	   
	      <!--- dispute-full-detail END-->
	   
	   
	   <div class="clearfix"></div>
	    </form>
	  </div><!--- modal-body-->
    </div>
  </div>
</div>

 <!---------- Light Box END----------->
 
 
 

<?php include('footer.php')?>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="js/morris.min.js"></script>
<script>

  var day_data = [
      {"period": "Late payments", "licensed": 20, "sorned": 10},
      {"period": "Inquiries", "licensed": 15, "sorned": 16},
      {"period": "Foreclosures", "licensed": 18, "sorned": 5},
      {"period": "Bankruptcies", "licensed": 9, "sorned": 12},
      {"period": "Collections", "licensed": 18, "sorned": 4},
      {"period": "Charge Offs", "licensed": 6, "sorned": 20},
      {"period": "Tax Liens", "licensed": 8, "sorned": 1},
      {"period": "Credit Errors", "licensed": 6, "sorned": 7},
    ];

    Morris.Bar({
	  ymax: 20,
      element: 'graph',
      data: day_data,
      xkey: 'period',
      ykeys: ['licensed', 'sorned'],
      labels: ['Licensed', 'SORN'],
	  barColors: ["#123f58", "#999999"],
      xLabelAngle: 80,
	  resize:'true'

    }); 

	
</script>	


