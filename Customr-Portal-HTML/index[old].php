<?php include('header.php')?>	
	<div class="container main-container account-pages">
	<div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		         <h4 class="sidebar-title">My Credit</h4>
				<ul class="nav side-nav"> 
				<li class="active"><a href="index.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li> <a href="credit-factors.php"><i class="ion-clipboard"></i> Credit Factors</a> </li>
				 <li> <a href="#"><i class="ion-document-text"></i> Credit Report</a></li>
				 <li><a href="negatives-detail.php"><i class="ion-arrow-graph-up-right"></i>Dispute Progress</a></li> 
				 <li><a href="upload-documents.php"><i class="ion-upload"></i> Upload Documents</a> </li> 
				 </ul> 
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>My Credit Score</h3>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  score-meter STARTS here ========--->
		
		<section class="score-meter">
		<div class="meter col-md-9">
		<div class="provided-by">
		<span class="col-md-5">Provided by: </span> 
		<img src="images/transu-large.png">
		</div>
		<div class="clearfix"></div>
		 <div class="score-count">760</div>
		 <div class="score-rating">Credit Rating : <strong>Excellent</strong></div>
		 <div class="progress-container">
		  <div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style="width: 82%;"></div>
		  <div class="clearfix"></div>
		 <div class="progress meter-progress">
		 <span class="poor"></span>
		 <span class="fair"></span>
		 <span class="average"></span>
		 <span class="good"></span>
		 <span class="excellent"></span>
		 </div>
		  <div class="clearfix"></div>
		 <ul class="nav progress-counting">
		 <li>300</li>
		 <li>580</li>
		 <li>640</li>
		 <li>700</li>
		 <li>750</li>
		 <li>850</li>
		  </ul>
          </div><!---- progress-container-->   
		<p class="updation text-center"><i class="ion-clock"></i> Updated June 21,2016 &nbsp;|&nbsp; Next Update in 6 days</p>
		
		</div><!---- meter Close here-->

		<div class="meter-des col-md-3">
		 <p><span class="excellent"></span> Excellent 750+</p>
		 <p><span class="good"></span>Good 700-749</p>
		 <p><span class="average"></span>Average 660-699</p>
		 <p><span class="fair"></span>Poor 580-659</p>
	     <p> <span class="poor"></span>Bad < 580</p>
		</div>
		
		<div class="clearfix"></div>
		<center>
		<a class="btn btn-primary btn-lg">View Entire Report</a>
        </center>
		</section>
		<!----============  score-meter END here ===========--->
		
		<div class="clearfix"></div>
		
		<div class="section-title">
		<h3>Dispute Progress</h3>
		</div>
		<div class="clearfix"></div>
		
		
		<!----==============  dispute-progress START ========--->
		<section class="dispute-progress padd_left_0">		
		<div class="col-md-4 nagative-pie-container ">
		  
		  <div class="nagative-pie">
        <h3 class="subtitle text-center"> Negative Removed</h3>			  
		 <div class="c100 p72 medium blue">
                    <span>33 <span class="small">%</span> <small>Complete</small></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>		
	      </div>
		  <div class="short-detail-nagatives">
		  <a href="#" class="removed">5 Removed</a> |  <a href="#" class="remaining"> 10 Remaining</a> 
		   </div>
         <div class="clearfix"></div>
             <center> 
		     <a class="btn btn-primary btn-md">Details of Negatives Items</a>
			 </center>
		    </div>
			
			<!---=== nagative-pie-container END ====-->			
		    <div class="col-md-8 nagatives-bargraph padd_right_0">
			   <h3 class="subtitle text-center"> Numbers of Negatives items</h3>		   
			   <div id="graph"></div>
				</div>
			<!---===== nagatives-bargraph END =====--->
			
			
			
		
		</section>
<!----===============  dispute-progress END =======--->
		
			
<!----===============  Disputed Table START =======--->
		<div class="clearfix"></div>
		
		<div class="section-title"><h3>What’s Being Disputed</h3></div>
		
		<section class="Disputed-data">
		<div class="table-responsive">
		<table class="table table-bordered">
		    <tr>
			<th>&nbsp;</th>
			<th><img src="images/transu-s.png"></th>
			<th><img src="images/experian-s.png"></th>
			<th><img src="images/equifax-s.png"></th>		   
		   </tr>
		   
		    <tr>
			<td>Last Round Date</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		    <tr>
			<td>Bureau response due date</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		   
		    <tr>
			<td> Number of items in dispute</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		  <tr>
			<td>Total items disputed</td>
			<td>&nbsp;</td>
            <td>&nbsp;</td>
			<td>&nbsp;</td>				
		   </tr>
		
			</table>
			</div>
         </section>
		 <div class="clearfix"></div>
<!----===============  Disputed Table END =======--->

<!----===============  Coordinator  Detail Start=======--->
		
		<div class="section-title"><h3>Customer Support</h3></div>
         <section class="Contacts"> 
		 
		 <div class=" Coordinator-desc">
		 <div class="col-md-6 col-sm-6"><i class="ion-iphone"></i> 123 -456-7890</div>
		 <div class="col-md-6 col-sm-6"><i class="ion-email"></i> Email@email.com</div>
		 </div>
		  </section>
<!--==== Coordinator END =====--->
		

		</div>
<!----===========  content-container END here ======--->	

</div><!----  Row END here --->
 </div><!----  Container END here --->
 
 <script>

  var day_data = [
      {"period": "Late payments", "licensed": 20, "sorned": 10},
      {"period": "Inquiries", "licensed": 15, "sorned": 16},
      {"period": "Foreclosures", "licensed": 18, "sorned": 5},
      {"period": "Bankruptcies", "licensed": 9, "sorned": 12},
      {"period": "Collections", "licensed": 18, "sorned": 4},
      {"period": "Charge Offs", "licensed": 6, "sorned": 20},
      {"period": "Tax Liens", "licensed": 8, "sorned": 1},
      {"period": "Other", "licensed": 6, "sorned": 7},
    ];

    Morris.Bar({
	  ymax: 20,
      element: 'graph',
      data: day_data,
      xkey: 'period',
      ykeys: ['licensed', 'sorned'],
      labels: ['Licensed', 'SORN'],
	  barColors: ["#123f58", "#999999"],
      xLabelAngle: 80,
	  resize:'true'

    }); 

	
</script>	

<?php include('footer.php')?>
