<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer Portal</title>
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<link href="../css/responsive.css" rel="stylesheet">
	<link href="../css/ionicons.min.css" rel="stylesheet">	
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
	  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
	<script src="../js/bootstrap-select.js"></script>
	<script src="../js/lightbox.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
	<script type="text/javascript" src="../js/canvasjs.min.js"></script>
    <script src="../js/morris.min.js"></script>
	<script src="../js/custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
  
 
  
<div class="main_wrapper">
<!----- TOP  header  STARTS here -------->

<header class="navbar navbar-static-top header navbar-fixed-side navbar-fixed-side-left" id="top" role="slider">
    <div class="container">
        <div class="navbar-header">
		<a href="../" class="navbar-brand"><img src="../images/logo.png" alt=""></a>
			<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false"> 
            <i class="ion-drag"></i> 
              </button>
		</div>	
<nav id="main-navbartop" class="hidden-sr"> 		
		<ul class="nav navbar-nav navbar-right loged-user"> 
		<li class=""> 
		<a href="#" class="dropdown-toggle">
		<img src="../images/user.png"> John Smith <i class="ion-ios-arrow-down"></i></a> 
		<ul class="dropdown-menu">
			<li><a href="../my-account/personal-profile.php"><i class="ion-person"></i> Personal Profile</a></li> 
			<li><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings</a></li> 
			<li><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li> 
			<li><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication</a></li> 
			<li class="divider"></li>
			<li><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			<li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
            <li class="divider"></li>	
            <li><a href="#"><i class="ion-log-out"></i> log out</a></li>			
			</ul>  
		</li>
		</ul>
		</nav>
	
    </div>
	<div class="clearfix"></div>
	
<nav class="main-navbar"> 
<div class="container">   
<button class="navbar-toggle toggle-close" type="button"><i class="ion-drag"></i> </button>  
	<ul class="nav navbar-nav main-menu"> 
	<li><a href="../index.php"> My Credit</a></li>
	 <li><a href="../tools.php"> Tools</a></li>
	 <li> <a href="../recommendations.php">Recommendations</a></li>
	 <li><a href="../credit-cards.php">Credit Cards</a> </li> 
	  <li><a href="../debt-help.php">Debt Help</a> </li> 
	 <li><a href="../loans.php">Loans</a></li> 
	 <li><a href="../insurance.php">Insurance</a> </li> 
	 </ul> 
 </div>
 </nav> 
 
 <!---  MOBILE -SIDEBAR - Menu  -->

<nav class="mobile-menu-sidebar"> 
<button class="navbar-toggle toggle-close" type="button"><i class="ion-drag"></i> </button>
	<ul class="nav navbar-nav mobile-menu">
       <li class="dropdown user"> 
			<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><img src="../images/user.png"> John Smith <i class="ion-ios-arrow-down"></i></a> 
			<ul class="dropdown-menu">
			<li><a href="../my-account/personal-profile.php"><i class="ion-person"></i> Personal Profile</a></li> 
			<li><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings</a></li> 
			<li><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li> 
			<li><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication</a></li> 
			<li class="divider"></li>
			<li><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			<li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
            <li class="divider"></li>	
            <li><a href="#"><i class="ion-log-out"></i> log out</a></li>			
			</ul> 
			</li>

	
	<li class="dropdown"> 
		<a  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> My Credit</a> 
		<ul class="dropdown-menu"> 
				<li><a href="../index.php"> Overview</a> </li>
				 <li> <a href="../my-credit/credit-factors.php"> Credit Factors</a> </li>
				 <li> <a href="../my-credit/credit-report.php"> Credit Report</a></li>
				 <li><a href="../my-credit/dispute-progress.php"> Dispute Progress</a></li> 
				 <li><a href="../my-credit/upload-documents.php"> Upload Documents</a> </li>  
		</ul> 
	</li>
	 <li class="dropdown"> 
		<a  href="tools.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Tools</a> 
		<ul class="dropdown-menu"> 
				<li><a href="../tools.php">Overview</a> </li>
			     <li><a href="../tools/credit-score-simulator.php">Credit Score Simulator </a> </li>
				<li><a href="../tools/home-calculator.php">Home Calculator</a> </li>
				 <li> <a href="../tools/debt-calculator.php">Debt Calculator</a> </li>
				 <li> <a href="../tools/loan-calculator.php">Loan Calculator</a></li>
				 <li><a href="../tools/amortization-calculator.php">Amortaization Calculator</a></li>  
		</ul> 
	</li>
	
	 <li> <a href="../recommendations.php">Recommendations</a></li>
	 <li><a href="../credit-cards.php">Credit Cards</a> </li> 
	  <li><a href="../debt-help.php">Debt Help</a> </li> 
	 
	 <li class="dropdown"> 
		<a  href="tools.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Loans</a> 
		<ul class="dropdown-menu"> 
				<li><a href="../loans.php">Overview</a> </li>
			     <li><a href="../loans/home-loans.php">Home Loans</a> </li>
				<li><a href="../loans/personal-loans.php">Personal Loans</a> </li>
				 <li> <a href="../loans/auto-loans.php">Auto Loans</a> </li>
				 <li> <a href="../loans/student-loans.php">Student Loans</a></li>
				 <li><a href="../loans/business-loans.php">Business Loans</a></li>  
		</ul> 
	</li>

	 <li class="dropdown"> 
		<a  href="tools.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Insurance</a> 
		<ul class="dropdown-menu"> 
				<li><a href="../insurance.php">Overview</a> </li>
			     <li><a href="../insurance/life-insurance.php">Life Insurance</a> </li>
				<li><a href="../insurance/auto-insurance">Auto Insurance</a> </li>
				<li> <a href="../insurance/home-insurance.php">Home Insurance</a> </li>
		</ul> 
	</li>
	 </ul> 
 </nav>	
 

</header>
<!----- TOP  header  END here -------->

<div class="clearfix"></div>