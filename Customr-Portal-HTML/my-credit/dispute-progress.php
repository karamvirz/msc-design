<?php include('../header-2.php')?>	
        <div class="container main-container">
	 <div class="row">
		 <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">My Credit</h4>
				<ul class="nav side-nav"> 
				<li><a href="../index.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li> <a href="../my-credit/credit-factors.php"><i class="ion-clipboard"></i> Credit Factors</a> </li>
				 <li> <a href="../my-credit/credit-report.php"><i class="ion-document-text"></i> Credit Report</a></li>
				 <li class="active"><a href="../my-credit/dispute-progress.php"><i class="ion-arrow-graph-up-right"></i>Dispute Progress</a></li> 
				 <li><a href="../my-credit/upload-documents.php"><i class="ion-upload"></i> Upload Documents</a> </li> 
				 </ul>  
				</div>
		
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		<div class="section-title"><h3>Dispute Progress</h3></div>
		<div class="section-title section-sub-title">
			<h3>You're Viewing <img src="../images/transu-s.png"></h3>
			<form class="pull-right form-inline bureaus">
			
			 <div class="form-group">
			    <select class="selectpicker">
				 <option data-icon="images/transu-s.png"></option>
				  <option>All 3 Bureau</option>				 
				  <option data-icon="images/experian-s.png"></option>
				  <option data-icon="images/equifax-s.png"></option>
				</select>		     
			   </div>
			 </form>
		</div>
		<div class="clearfix"></div>

		<!----==============  dispute-progress START ========--->
		<section class="dispute-progress">		
		<div class="col-md-6 nagative-pie-container">
		
		<div class="nagative-pie col-md-6 col-sm-6 col-xs-6">	
         <h3 class="subtitle text-center"> Negatives Started With</h3>		
		 <div class="c100 p65 medium blue">
                    <span>15 <small>Negatives</small></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>		
	      </div>
		  
		  <div class="nagative-pie col-md-6 col-sm-6 col-xs-6">
        <h3 class="subtitle text-center"> Negatives Repaired</h3>			  
		 <div class="c100 p50 medium orange">
                    <span>5 <small>Repaired</small></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>		
	      </div>
		  <div class="clearfix"></div>	
		  
		  <div class="nagative-pie col-md-6 col-sm-6 col-xs-6">
        <h3 class="subtitle text-center"> In Dispute</h3>			  
		 <div class="c100 p10 medium green">
                    <span>10 <small>In Dispute</small></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>		
	      </div>
		  
		  
		  <div class="nagative-pie col-md-6 col-sm-6 col-xs-6">
        <h3 class="subtitle text-center"> Percent Complete</h3>			  
		 <div class="c100 p15 medium dark-blue">
                    <span>33 <span class="small">%</span> <small>Complete</small></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>		
	      </div>
         <div class="clearfix"></div>	
		    </div>
			
			<!---=== nagative-pie-container END ====-->			
		    <div class="col-md-6 nagatives-bargraph padd_right_0">
			   <h3 class="subtitle text-center"> Numbers of Negatives items</h3>		   
			   <div id="graph"></div>
				</div>
			<!---===== nagatives-bargraph END =====--->

		
		</section>
<!----===============  dispute-progress END =======--->
		
			
<!----===============  Disputed Table START =======--->
		<div class="clearfix"></div>
		
		<div class="section-title"><h3>What’s Being Disputed</h3></div>
		
		<section class="Disputed-data">
		<div class="table-responsive">
		<table class="table table-bordered">
		    <tr>
				<th>&nbsp;</td>
				<th><img src="../images/transu-s.png"></th>
				<th><img src="../images/experian-s.png"></th>
				<th><img src="../images/equifax-s.png"></th>		   
		   </tr>
		   
		    <tr>
				<td>Last Round Date</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>				
		   </tr>
		    <tr>
				<td>Bureau response due date</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>				
		   </tr>
		   
		    <tr>
				<td>Number of items in dispute</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>				
		   </tr>
		  <tr>
				<td>Total items disputed</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>				
		   </tr>
		
		</table>
		 </div>
         </section>
		 <div class="clearfix"></div>
<!----===============  Disputed Table END =======--->

<!----===============  Nagative  Detail Start=======--->
		
		<div class="section-title"><h3>Dispute Details</h3></div>
         <section class="nagative-details"> 
			 <div class="table-responsive">
			 <table class="table nagative-details-table table-hover">
				 <thead>
					 <th>Account Name</th>
					 <th> Account Number</th>
					 <th><select class="form-control fancy-select input-sm">
       					  <option>Account Type</option>
       					  <option> Credit Card </option>
       					  <option>Auto</option>
       					  <option>Other</option>						 
						 <select>
						 </th>
					 <th><select class="form-control fancy-select input-sm">
       					  <option>Negative Type</option>
       					  <option>Loream Ipsum </option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>						 
						 <select>					 				 
					   </th>
					 <th><select class="form-control fancy-select input-sm">
       					  <option>Dispute Reason</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>
       					  <option>Loream Ipsum</option>						 
						 <select>
					 </th>
					 
					 <th class="dispute-status">
					 <small>Dispute Status</small>
					 <div class="clearfix"></div>
					 <img src="../images/transu-s.png"/>					 
					 </th>
				 </thead>
				 <tbody>
					 <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td>Loream Ipsum</td>						
						 <td>In dispute</td>
						
					 </tr>	
					 <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-model-s"></i> Credit Card </td>
						 <td>Late payment </td>						
						  <td>Loream Ipsum</td>
						 <td><span class="nagative-removed">Removed</span></strong></td>
						
					 </tr>
					 	<tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						 <td>Loream Ipsum</td>						
						 <td>In dispute</td>
						 
					 </tr>
					  <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-model-s"></i> Credit Card </td>
						 <td>Late payment </td>
						 <td>Late payment </td>						 
						 <td><span class="nagative-removed">Removed</span></strong></td>
						
					 </tr>
					  <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td>Loream Ipsum</td>						 
						 <td>In dispute</td>
						
					 </tr>	
					 <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-model-s"></i> Credit Card </td>
						 <td>Late payment </td>						
						  <td>Loream Ipsum</td>
						 <td><span class="nagative-removed">Removed</span></strong></td>
						
					 </tr>
					 	<tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>						
						 <td>Loream Ipsum</td>
						 <td>In dispute</td>
						 
					 </tr>
					  <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-model-s"></i> Credit Card </td>
						 <td>Late payment </td>						
						  <td>Loream Ipsum</td>
						 <td><span class="nagative-removed">Removed</span></strong></td>
						
					 </tr>
					 <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-card"></i> Credit Card </td>
						 <td>Late payment </td>
						  <td>Loream Ipsum</td>						 
						 <td>In dispute</td>
						
					 </tr>
					  <tr>
						 <td><a href="#">Loream Ipsum</a></td>
						 <td>58924528582</td>
						 <td><i class="ion-model-s"></i> Credit Card </td>
						 <td>Late payment </td>						
						 <td>Loream Ipsum</td>
						 <td><span class="nagative-removed">Removed</span></strong></td>
					
					 </tr>
                     					 
				 </tbody>
			 </table>
			 
			 
			 </div><!--- table Responsive-->
			 
		<nav class="pagination-nav"> 	
        <div class="col-md-3  padd_0">
		  <label class="col-md-6 col-xs-6 padd_0 show-rows">Show rows:</label>
		  <div class="col-md-4 col-xs-6 padd_0">
			  <select class="form-control fancy-select input-sm">
							  <option>10</option>
							  <option>25</option>
							  <option>50</option>						 
							  <option>100</option>						 
							 </select>
		     </div>	   
		    </div>		 
			 <ul class="pagination pull-right"> 
				 <li class="disabled"><a href="#" aria-label="Previous">
				 <i class="ion-ios-arrow-thin-left"></i></a></li> 
				 <li class="active"><a href="#">1</a></li> 
				 <li><a href="#">2</a></li> 
				 <li><a href="#">3</a></li> 
				 <li><a href="#">4</a></li> 
				 <li><a href="#">5</a></li>
				 <li><a href="#" aria-label="Next"><i class="ion-ios-arrow-thin-right"></i></a></li>
			 </ul> </nav>
			 
			 
			 
			 
			 
		  </section>
		  
<!----===============  Nagative  Detail END=======--->
		

		</div><!----===========  content-container END here ======--->	
   </div><!----  Row END here --->
 </div><!----  Container END here --->
 
<?php include('../footer-2.php')?>	
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="js/morris.min.js"></script>
<script>

  var day_data = [
      {"period": "Late payments", "licensed": 20, "sorned": 10},
      {"period": "Inquiries", "licensed": 15, "sorned": 16},
      {"period": "Foreclosures", "licensed": 18, "sorned": 5},
      {"period": "Bankruptcies", "licensed": 9, "sorned": 12},
      {"period": "Collections", "licensed": 18, "sorned": 4},
      {"period": "Charge Offs", "licensed": 6, "sorned": 20},
      {"period": "Tax Liens", "licensed": 8, "sorned": 1},
      {"period": "Other", "licensed": 6, "sorned": 7},
    ];

    Morris.Bar({
	  ymax: 20,
      element: 'graph',
      data: day_data,
      xkey: 'period',
      ykeys: ['licensed', 'sorned'],
      labels: ['Licensed', 'SORN'],
	  barColors: ["#123f58", "#999999"],
      xLabelAngle: 80,
	  resize:'true'		

    }); 

</script>
