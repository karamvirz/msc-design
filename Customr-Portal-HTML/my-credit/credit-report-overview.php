<div class="row">

               <div class="meter col-md-5">
					<div class="provided-by">
					<span class="col-md-5">Provided by: </span> 
					<img src="../images/transu-large.png">
					</div>
					<div class="clearfix"></div>
					 <div class="score-count">760</div>
					 <div class="score-rating">Credit Rating : <strong>Excellent</strong></div>
					 <div class="progress-container">
					  <div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style="width: 82%;"></div>
					  <div class="clearfix"></div>
					 <div class="progress meter-progress">
						 <span class="poor"></span>
						 <span class="fair"></span>
						 <span class="average"></span>
						 <span class="good"></span>
						 <span class="excellent"></span>
					 </div>
					  <div class="clearfix"></div>
					 <ul class="nav progress-counting">
						 <li>300</li>
						 <li>580</li>
						 <li>640</li>
						 <li>700</li>
						 <li>750</li>
						 <li>850</li>
					  </ul>
					  </div><!---- progress-container-->   
		               <p class="updation text-center">
					   <small><i class="ion-clock"></i> Updated June 21,2016 &nbsp;|&nbsp; Next Update in 6 days</small></p>
					   
					   
					   <div class="clearfix"></div><hr>
					     <h3 class="subtitle text-center"> Credit Utilization </h3>
					   <div class="Utilization-pie-chart col-md-8 col-md-offset-2">							
						   <div id="chartContainer1" style="height:180px;width:100%;"></div>						   
			           </div> 
					   
					   
		    </div><!---- meter Close here-->
			
			<div class="col-md-7 report-overview-right">	
			
			 <!---div id="line-chart"></div-->
			 
			 <div class="custom-chart">
			 
			 <div class="custom-chart-barbox">
			  <table class="table custom-chart-bartable">
			    <tr>
				<td></td>
			      <td>                   
				 <table class="table">	
                     <tr>	
                     <td>					 
					 
					<div class="progress progress-bar-vertical light custom">
					<div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="height: 20%;">
					  <div class="bar-tooltip">Score on 05 Jul 2016 : 550</div>
					</div>
				  </div>
				  <span class="month">Jan</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical light custom">
					<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="height: 70%;">
					  <div class="bar-tooltip">Score on 05 Feb 2016 : 730</div>
					</div>
				  </div>
				   <span class="month">Feb</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical light custom">
					<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="height: 60%;">
					   <div class="bar-tooltip">Score on 05 March 2016 : 699</div> 
					</div>
				  </div>
				   <span class="month">Mar</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical light custom">
					<div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="height: 30%;">
					  	   <div class="bar-tooltip">Score on 05 April 2016 : 600</div> 
					</div>
				  </div>
				   <span class="month">Apr</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical light custom">
					<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="height: 80%;">
					 <div class="bar-tooltip">Score on 05 May 2016 : 750</div>   
					</div>
				  </div>
				   <span class="month">May</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical light custom">
					<div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="height: 40%;">
					  	 <div class="bar-tooltip">Score on 05 June 2016 : 650</div>   
					</div>
				  </div>
				   <span class="month">June</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical custom">
					<div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="height:90%;">
					  	 <div class="bar-tooltip">Score on 05 July 2016 : 780</div>   
					</div>
				  </div>
				   <span class="month">July</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical">
					<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="height: 0%;">
					  
					</div>
				  </div>
				   <span class="month">Aug</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical">
					<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="height: 0%;">
					  
					</div>
				  </div>
				   <span class="month">Sep</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical">
					<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="height: 0%;">
					  
					</div>
				  </div>
				   <span class="month">Act</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical">
					<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="height: 0%;">
					  
					</div>
				  </div>
				   <span class="month">Nov</span>
				   </td>
				   </tr>
			      </table>
				  
				  <table class="table">	
                     <tr>	
                     <td>					 
					<div class="progress progress-bar-vertical">
					<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="height: 0%;">
					  
					</div>
				  </div>
				   <span class="month">Dec</span>
				   </td>
				   </tr>
			      </table>
				  
			      </td>
				  
				  
				  
				  
				  
			     </tr>
			   </table>
			   </div>
			 <table class="table custom-chart-table">
			 <tbody>
			 <tr><td class="excellent"><span>Excellent</span> <small>750+</small> </td><td colspan="13">&nbsp;</td></tr>
			 <tr><td class="good"><span>Good</span> <small>700 - 749 </small></td><td colspan="13">&nbsp;</td></tr>
			 <tr><td class="average"><span>Average</span> <small>650 – 699</small>  </td><td colspan="13">&nbsp;</td></tr>
			 <tr><td class="poor"><span>Poor</span> <small>550 – 649 </small></td><td colspan="13">&nbsp;</td></tr>
			 <tr><td class="bad"><span>Bad</span> <small>< 550</small> </td><td colspan="13">&nbsp;</td></tr>
			
			   </tbody>
			   <tfoot>
			   <tr>
			   <td>&nbsp;</td><td>Jan</td><td>FEB</td><td>Mar</td><td>Apr</td><td>May</td><td>june</td><td>July</td><td>Aug</td><td>Sep</td><td>Aug</td><td>Act</td><td>Nov</td><td>Dec</td>			   
				 </tr>
			    </tfoot>
			    </table>
			    </div>	 
			 
			 
			 </div>
            <div class="clearfix"></div>			  
			 </div>	<!--- ROW -->

<div class="clearfix"></div><br><hr/>


					 <div class="text-center report-amount row">
                     <figure class="col-sm-4 col-xs-4"> 					 
						  <h4>Derogatory Accounts</h4>
						   <h2>1</h2>
					    </figure>
						
					 <figure class="col-sm-4 col-xs-4">  
						 <h4>Payment History </h4>
						 <h2>99.3%</h2>
					 </figure> 
					 
					  <figure class="col-sm-4 col-xs-4">  						
						  <h4>Age of Credit</h4>
						  <h2>6Yrs 1M</h2>
					   </figure> 
					 
					 
					 </div>	<!--- ROW -->
		
			 <div class="clearfix"></div><hr><br>
			 
			
			<div class="row">
			
			  <div class="col-md-4 col-md-offset-1">			
				<div id="chartContainer2" style="height:250px; width:100%;"></div>						   
			        </div> 
			      
			  
			  <div class="pie-des padd_right_0 col-md-5 col-md-offset-1">
				   <table class="table">
					<tr><td><span class="maroon"></span>Credit Cards</td><td>4</td></tr>				 
					 <tr><td><span class="grey"></span>Mortgage</td><td>0</td></tr>			 
					 <tr><td><span class="blue"></span>Auto</td><td>3</td></tr>			 
					 <tr><td><span class="grey"></span>Student</td><td>0</td></tr>			 
					 <tr><td><span class="grey"></span>Other </td><td>0</td></tr>			 
					 <tr class="total"><td><span class="grey"></span>Total Accounts </td><td>7</td></tr>			 
					</table>
				   </div>
					 
					 
			 
			 
			 
					 </div>	<!--- ROW -->
					 
					 	 <div class="clearfix"></div><br><br><br>
		
		<script>
	
window.onload = function () {
	 CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#ffbe10",
                "#0193b6"
                ]);
	var chart = new CanvasJS.Chart("chartContainer1",
	{
		//title:{
			//text: " Principal Payment Chart ",
		//},
        animationEnabled: true,
		legend: {
			//verticalAlign: "center",
			//horizontalAlign: "right"
			 fontSize: 12,
		},
		 colorSet: "greenShades",
		//theme: "theme1",
		data: [
		{        
			type: "pie",			
			indexLabelFontFamily: "Roboto",       
			indexLabelFontSize: 16,
			indexLabelFontWeight: "normal",
			startAngle:0,
			indexLabelFontColor: "#fff",       
			indexLabelLineColor: "#0193b6", 
			indexLabelPlacement: "inside", 
			toolTipContent: "{name}: ${y}00",
			showInLegend: true,
			indexLabel: "#percent%", 
			dataPoints: [
              //{  y: 52, name: "Time At Work", legendMarkerType: "triangle"},
              {  y: 10, name: "Credit Used", legendMarkerType: "circle"},
              {  y: 90, name: "Available Credit", legendMarkerType: "circle"}
			]
		}
		]
	});
	chart.render();
	
	var chart = new CanvasJS.Chart("chartContainer2",
	{
		//title:{
			//text: " Principal Payment Chart ",
		//},
        animationEnabled: true,
		legend: {
			//verticalAlign: "center",
			//horizontalAlign: "right"
			fontSize: 12,
		},
		 //colorSet: "greenShades",
		theme: "theme1",
		data: [
		{        
			type: "pie",
			
			indexLabelFontFamily: "roboto",       
			indexLabelFontSize: 16,
			indexLabelFontWeight: "normal",
			startAngle:0,
			indexLabelFontColor: "#fff",       
			indexLabelLineColor: "#0193b6", 
			indexLabelPlacement: "inside", 
			toolTipContent: "{name}: {y}",
			showInLegend: false,
			indexLabel: "#percent%", 
			dataPoints: [
              //{  y: 52, name: "Time At Work", legendMarkerType: "triangle"},
              {  y: 3, name: "Auto accounts", legendMarkerType: "square"},
              {  y: 4, name: "Credit Card Accounts", legendMarkerType: "circle"}
			]
		}
		]
	});
	chart.render();
}

</script>
 