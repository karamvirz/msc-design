<?php include('../header-2.php')?>	
	<div class="container main-container">
	 <div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">My Credit</h4>
				<ul class="nav side-nav"> 
				<li><a href="index.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li> <a href="../my-credit/credit-factors.php"><i class="ion-clipboard"></i> Credit Factors</a> </li>
				 <li  class="active"> <a href="../my-credit/credit-report.php"><i class="ion-document-text"></i> Credit Report</a></li>
				 <li><a href="../my-credit/dispute-progress.php"><i class="ion-arrow-graph-up-right"></i>Dispute Progress</a></li> 
				 <li><a href="../my-credit/upload-documents.php"><i class="ion-upload"></i> Upload Documents</a> </li> 
				 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Dispute Inaccuracies</h3>
		<form class="pull-right form-inline">	
		 <a href="../my-credit/credit-report.php" class="btn btn-default btn-sm">
		 <i class="ion-ios-arrow-thin-left"></i> Back</a>
		 </form>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  credit-report STARTS here ========--->
		
		<section class="">
		 <div class="text-center col-md-8 col-md-offset-2">
		 <p>Reference site about Lorem Ipsum, giving information on its origins, <br>as well as a random Lipsum generator.</p>
		 </div>
		 
		 <div class="clearfix"></div><br>
		 
		 <div class="table-responsive">		
	   
	   <table class="table nagative-details-table table-hover dispute-inaccuracies table-collapse">
				 <thead>
					 <tr>
					 <th>
					 <div class="checkbox fancy_checkbox">
						<label><input type="checkbox" id="select-all"><span></span></label>
			           </div>
					   </th>
					 <th>Account Name</th>					   
					 <th>Select Reason</th>	
                     <th>Dispute  Status</th>					 
					 <th> Date Reported</th>
					 <th>Date Opened</th>
					 <th>Derogatory Type</th>
					 
						<th>
							<div class="dropdown">
							  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
								Type
								<span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
											 <li>All</li>			  
											  <li>Line of credit</li>							  
											  <li>Installment</li>
											  <li>Mortgage</li>						 
											  <li>Open</li>						 
											  <li>Revolving</li>						 
											  <li>Unknown</li>
							  </ul>
							</div>			
			
			      </th>
				  <th>
					<div class="dropdown">
					  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
						ECOA
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">	
									  <li>All</li>			  
									  <li>Authorized</li>							  
									  <li>Co-maker</li>
									  <li>Individual</li>						 
									  <li>Joint</li>						 
									  <li>Maker</li>						 
									  <li>Shared</li>						 
									  <li>Terminated</li>						 
									  <li>Undesignated</li>						 
									  <li>Deceased</li>	
					  </ul>
					</div>
	        </th>
						
						
						
				 </tr>
				 </thead>
				 <tbody>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>Open</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr><tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr><tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						 
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>Open</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						 <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
					
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>Open</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>				  
						
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>Open</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>						  
						
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>Open</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 <tr>
						 <td>
						  <div class="checkbox fancy_checkbox">
							<label><input type="checkbox"><span></span></label>
						   </div>
					    </td>
						 <td class="account-name"><a href="#"><strong>Loream Ipsum</strong></a><p><strong> 30343434</strong></p></td>
						 <td class="info">
						   <div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Dispute Reason
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu " aria-labelledby="dLabel">
			                   <li>All</li>
							   <li>Did not authorize inquiry</li>
								  <li>Personal information inaccurate</li>
								  <li>Verify account balance </li> 
								  <li>Verify date opened </li> 
								  <li>Verify date of last payment </li> 
								  <li>Verify date of delinquency </li> 
								  <li>Never been late </li> 
								  <li>No knowledge of account </li>
								  <li>Belongs to someone else with similar info </li> 
								 <li> Not mine </li> 
								  <li>ID theft </li>
			     </ul>
			    </div>	
						 </td>
						  <td>In Dispute</td>
						 <td>Jan 05, 2016 </td>
						 <td>Jan 05, 2016 </td>
						 <td>Late payment </td>					  
						
						 <td> Mortage</td>
						 <td> Maker</td>
						 						
						
					 </tr>
					 
					 
					 
					
					 
					 
					 
					 
					 </tbody> 
					 </table> 
					   </div>
					   
					   
			<div class="clearfix"></div>
			
     <nav class="pagination-nav"> 	
			<div class="col-md-3  padd_0">
			  <label class="col-md-6 col-xs-6 padd_0 show-rows">Show rows:</label>
			  <div class="col-md-4 col-xs-6 padd_0">
				  <select class="form-control fancy-select input-sm">
							  <option>5</option>
							  <option>10</option>
							  <option>25</option>
							  <option>50</option>						 
							  <option>100</option>						 
							 </select>	
				 </div>	   
				</div>		 
				 <ul class="pagination pull-right"> 
					 <li class="disabled"><a href="#" aria-label="Previous">
					 <i class="ion-ios-arrow-thin-left"></i></a></li> 
					 <li class="active"><a href="#">1</a></li> 
					 <li><a href="#">2</a></li> 
					 <li><a href="#">3</a></li> 
					 <li><a href="#">4</a></li> 
					 <li><a href="#">5</a></li>
					 <li><a href="#" aria-label="Next"><i class="ion-ios-arrow-thin-right"></i></a></li>
				 </ul> 
			 </nav>
			 <div class="clearfix"></div><hr>
			 
			 <div class="dispute-light-bottom">		
    <div class="checkbox fancy_checkbox col-md-5 padd_0 fancy_checkbox-sm">
				<label>
				  <input type="checkbox"> <span>I agree to dispute all checked items</span>
				</label>
			  </div>
            <div class="col-md-3 padd_0"><a class="btn btn-md btn-primary btn-block">Start Disputing</a></div>
             </div>
				  
		
		</section>
		<!----============  credit-report END here ===========--->
		
		
		
</div>
<!----===========  content-container END here ======--->	

</div><!----  Row END here --->
 </div><!----  Container END here --->
<script>
// SELECT ALL

$('#select-all').click(function(event) {   
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;                        
        });
    }
	 else{
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = false;                        
        });
    }
});
</script>
 
<?php include('../footer-2.php')?>