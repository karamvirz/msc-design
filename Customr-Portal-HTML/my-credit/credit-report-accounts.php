<a class=" pull-right btn-sm" data-toggle="modal" data-target="#Edit-Accounts" href="#">
<i class="ion-edit"></i> Edit columns</a>
<div class="clearfix"></div>
<div class="table-responsive">
<!-- Button trigger modal -->

<table class="table less-border-table table-collapse possitive-accounts table-hover" id="possitive-accounts">
    <thead>
         <tr class="headerrow">
		    <th> Account Name</th>
            <th> Date Reported</th>
            <th>Date Opened</th>
            <th>Limit</th>
            <th>Balance</th>
            <th>
			<div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Status
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" aria-labelledby="dLabel">
			                   <li>All</li>
							  <li>Closed</li>							  
							  <li>Open</li>
							  <li>Paid</li>						 
							  <li>Refinanced</li>						 
							  <li>Transferred</li>						 
							  <li>Unknown</li>
			  </ul>
			</div>			
							 
							 
							 </th>
            <th>
			<div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				Type
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" aria-labelledby="dLabel">				         
							  <li>All</li>							  
							  <li>Line of credit</li>							  
							  <li>Installment</li>
							  <li>Mortgage</li>						 
							  <li>Open</li>						 
							  <li>Revolving</li>						 
							  <li>Unknown</li>
			  </ul>
			</div>			
			
			</th>
			
            <th>
			<div class="dropdown">
			  <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" class="btn">
				ECOA
				<span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">	
                               <li>All</li>			  
							  <li>Authorized</li>							  
							  <li>Co-maker</li>
							  <li>Individual</li>						 
							  <li>Joint</li>						 
							  <li>Maker</li>						 
							  <li>Shared</li>						 
							  <li>Terminated</li>						 
							  <li>Undesignated</li>						 
							  <li>Deceased</li>	
			  </ul>
			</div>
	        </th>
            <th>&nbsp;</th>
        </tr>
    </thead>
	
    <tbody>
	
	
<!----==============   Collapse  =================--->	

     <tr data-toggle="collapse" data-target="#possitive-account1" class="accordion-toggle">
		
				<td class="account-name"><a href="#"><strong>Loream Ipsum</strong>
					<p><strong> 30343434</strong></p></td>
				<td>Jan 05, 2016 </td>
				<td>Jan 05, 2016 </td>
				<td>$10,000</td>
				<td><strong> $0</strong></td>
				<td>Opened</td>
				<td> Mortage</td>
				<td> Maker</td>
				 <td><button class="btn btn-default btn-xs" title="View Details"><i class="ion-eye"></i></button></td>
          </tr>
           <tr>
			 <td colspan="12" class="hiddenRow">
				 <div class="accordian-body collapse" id="possitive-account1"> 
				 

		    <div class="col-md-6 col-sm-6">
			<h5>Accounts Detail <button data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="right" data-toggle="popover" data-container="body" class="btn help" type="button" data-original-title="" title=""> <i class="ion-information-circled"></i></button></h5>
			<table class="table  table-accounts-detail">
			<tbody><tr>
				<td>Last Reported</td>
				<td>June 17, 2016</td>			
			</tr>
			<tr>
				<td>Credit Name</td>
				<td>CAPITAL ONE</td>			
			</tr>
			<tr>
				<td>Account Type</td>
				<td>June 17, 2016</td>			
			</tr>
			<tr>
				<td>Account Status</td>
				<td>Open</td>			
			</tr>
			<tr>
				<td>Opened Date</td>
				<td>Nov 03,2012</td>			
			</tr>
			<tr>
				<td>Closed Date</td>
				<td>--</td>			
			</tr>
			<tr>
				<td>Limit</td>
				<td>$85,00</td>			
			</tr>
			   <tr>
    <td>Balance</td>
    <td>$179</td>   
    </tr>

    <tr>
    <td>Highest Balance</td>
    <td>$5,978</td>   
    </tr>
    
    <tr>
    <td>Payment Status</td>
    <td>Current</td>   
    </tr>

    <tr>
    <td>Worst Payment Status</td>
    <td>Current</td>   
    </tr>

    <tr>
    <td>Date of Last Payment</td>
    <td>June 16,2016</td>   
    </tr>
   
    <tr>
    <td>Amount Past Due</td>
    <td>$0</td>   
    </tr>

    <tr>
    <td>Times 30/60/90 Days Late</td>
    <td>0/0/0</td>   
    </tr>

    <tr>
    <td>Remarks</td>
    <td>--</td>   
    </tr>			
			
			
            </tbody></table>			
			  </div>
			  
			  
			<div class="col-md-6 col-sm-6">
			  <h5>Payment History</h5>
			  <h6>Latest Status: Current <button data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="right" data-toggle="popover" data-container="body" class="btn help" type="button" data-original-title="" title=""> <i class="ion-information-circled"></i></button></h6>
			  
		<div class="table-responsive">	  
			<table class="table table-payment-history">
			<tbody><tr>
				<th>2016</th>
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
			</tr>
			<tr>
				<th>2016</th>
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>	
                <td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>
                <td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>				
			</tr>
			<tr>
				<th>2016</th>
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>	
                <td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>
                <td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>				
			</tr>
			<tr>
				<th>2016</th>
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>	
                <td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>
                <td><span class="label label-success">Ok</span></td>			
				<td><span class="label label-success">Ok</span></td>				
			</tr>
			<tr>
				<th>2016</th>
				<td><span class="label label-success">Ok</span></td>						
			</tr>
			
			</tbody><tfoot>
			<tr><td>&nbsp;</td>
			<td>J</td><td>F</td><td>M</td><td>A</td><td>M</td><td>J</td><td>J</td><td>A</td><td>S</td>
			<td>A</td><td>N</td><td>D</td>
			</tr>
			</tfoot>
            </table>
             </div>	
         <div class="clearfix"></div><hr>		 
			 
		 <div class="nagative-pie">		
         <h5 class="subtitle text-center">  Credit Utilization :</h5>		
		 <div class="c100 p3 small blue">
                    <span>3.5<span class="small">%</span></span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>		
	      </div>
		  
	<div class="clearfix"></div><hr>			
			<h5>Creditor Contact Details</h5>
			<address>			
			<p>Loream Ipsum Dollar</p>
			<p>PO Box 3021</p>
			<p>Amate Dollar, Dollar</p>			
			<p>25125623</p>			
			 <address>		 

			  </address></address></div><!----Col-md-6-->

						 
				 
				  </div>              			  
				  </td>
			   </tr>			   
 <!----==============   Collapse END =================--->	    
 <!----==============   Collapse  =================--->	

     <tr data-toggle="collapse" data-target="#possitive-account2" class="accordion-toggle">
		
				<td class="account-name"><a href="#"><strong>Loream Ipsum</strong>
					<p><strong> 30343434</strong></p></td>
				<td>Jan 05, 2016 </td>
				<td>Jan 05, 2016 </td>
				<td>$10,000</td>
				<td><strong> $0</strong></td>
				<td>Opened</td>
				<td> Mortage</td>
				<td> Maker</td>
				 <td><button class="btn btn-default btn-xs" title="View Details"><i class="ion-eye"></i></button></td>
          </tr>
           <tr>
			 <td colspan="12" class="hiddenRow">
				 <div class="accordian-body collapse" id="possitive-account2"> 
				        
                 <h4>content here</h4>						
				  </div>              			  
				  </td>
			   </tr>			   
 <!----==============   Collapse END =================--->	
 
 <!----==============   Collapse  =================--->	

     <tr data-toggle="collapse" data-target="#possitive-account3" class="accordion-toggle">
		
				<td class="account-name"><a href="#"><strong>Loream Ipsum</strong>
					<p><strong> 30343434</strong></p></td>
				<td>Jan 05, 2016 </td>
				<td>Jan 05, 2016 </td>
				<td>$10,000</td>
				<td><strong> $0</strong></td>
				<td>Opened</td>
				<td> Mortage</td>
				<td> Maker</td>
				 <td><button class="btn btn-default btn-xs" title="View Details"><i class="ion-eye"></i></button></td>
          </tr>
           <tr>
			 <td colspan="12" class="hiddenRow">
				 <div class="accordian-body collapse" id="possitive-account3"> 
				  <h4>content here</h4>
				              
				  </div>              			  
				  </td>
			   </tr>			   
 <!----==============   Collapse END =================--->	
 
 <!----==============   Collapse  =================--->	

     <tr data-toggle="collapse" data-target="#possitive-account4" class="accordion-toggle">
		
				<td class="account-name"><a href="#"><strong>Loream Ipsum</strong>
					<p><strong> 30343434</strong></p></td>
				<td>Jan 05, 2016 </td>
				<td>Jan 05, 2016 </td>
				<td>$10,000</td>
				<td><strong> $0</strong></td>
				<td>Opened</td>
				<td> Mortage</td>
				<td> Maker</td>
				 <td><button class="btn btn-default btn-xs" title="View Details"><i class="ion-eye"></i></button></td>
          </tr>
           <tr>
			 <td colspan="12" class="hiddenRow">
				 <div class="accordian-body collapse" id="possitive-account4"> 
				  <h4>content here</h4>
				              
				  </div>              			  
				  </td>
			   </tr>			   
 <!----==============   Collapse END =================--->	
 
 <!----==============   Collapse  =================--->	

     <tr data-toggle="collapse" data-target="#possitive-account5" class="accordion-toggle">
		
				<td class="account-name"><a href="#"><strong>Loream Ipsum</strong>
					<p><strong> 30343434</strong></p></td>
				<td>Jan 05, 2016 </td>
				<td>Jan 05, 2016 </td>
				<td>$10,000</td>
				<td><strong> $0</strong></td>
				<td>Opened</td>
				<td> Mortage</td>
				<td> Maker</td>
				 <td><button class="btn btn-default btn-xs" title="View Details"><i class="ion-eye"></i></button></td>
          </tr>
           <tr>
			 <td colspan="12" class="hiddenRow">
				 <div class="accordian-body collapse" id="possitive-account5"> 
				  <h4>content here</h4>
				              
				  </div>              			  
				  </td>
			   </tr>			   
 <!----==============   Collapse END =================--->	
 
 <!----==============   Collapse  =================--->	

     <tr data-toggle="collapse" data-target="#possitive-account6" class="accordion-toggle">
		
				<td class="account-name"><a href="#"><strong>Loream Ipsum</strong>
					<p><strong> 30343434</strong></p></td>
				<td>Jan 05, 2016 </td>
				<td>Jan 05, 2016 </td>
				<td>$10,000</td>
				<td><strong> $0</strong></td>
				<td>Opened</td>
				<td> Mortage</td>
				<td> Maker</td>
				 <td><button class="btn btn-default btn-xs" title="View Details"><i class="ion-eye"></i></button></td>
          </tr>
           <tr>
			 <td colspan="12" class="hiddenRow">
				 <div class="accordian-body collapse" id="possitive-account6"> 
				     <h4>content here</h4>          
				  </div>              			  
				  </td>
			   </tr>			   
 <!----==============   Collapse END =================--->	
 
 
       
      
       
    </tbody>
</table>

 </div>    




			
			
			
			
			
	<!-- Modal -->
<div class="modal fade edit-columns" id="Edit-Accounts" tabindex="-1" role="dialog" aria-labelledby="Edit-AccountsLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Columns to display</h4>
      </div>
      <div class="modal-body">
        <form> 
		<div class="input-group"> 
			<div class="input-group-addon">
			<i class="ion-search"></i></div> 
			<input type="text" class="form-control" id="exampleInputAmount" placeholder="Search here">
		</div> 
		</form>
		<div class="clearfix"></div><br>
		
		<div class="select-all col-md-12"><a href=""> Select all columns</a> | <a href="">Deselect all columns</a></div>
		
		<div class="col-md-4">

		<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Account Name</span>
			</label>
			</div>
		
          <div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Date Reported </span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Date Opened</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Limit</span>
			</label>
			</div>			

           <div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span>   Balance</span>
			</label>
			</div>	
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span>  Status</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Type </span>
			</label>
			</div>
		
		</div>
		
		
		<div class="col-md-4"> 





		<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> ECOA</span>
			</label>
			</div>
		
          <div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Amount Past Due</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Last payment date</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Terms</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Worst Status</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Times Late</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span>  Date Account Removed</span>
			</label>
			</div>
			
		
		
		</div>
		
		
		
		<div class="col-md-4">

		<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span>Remarks</span>
			</label>
			</div>
		
          <div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Payment History</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Creditors Contact Info</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Disputed</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Disputation Result</span>
			</label>
			</div>
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span> Date First Disputed</span>
			</label>
			</div>			
			<div class="checkbox fancy_checkbox fancy_checkbox-xs">
			<label>
			<input type="checkbox" checked><span>  Date Last Disputed</span>
			</label>
			</div>			

		
		</div>

		
		<div class="clearfix"></div>
      </div>
      <div class="modal-footer">
	     <button type="button" class="btn btn-primary-outline">Save changes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	



  <script src="../js/jquery-ui.min.js"></script>
   <script src="../js/jquery.sorttable.js"></script>	
	 <script>
       
$('.table-collapse').sorttable({
}).disableSelection();

    </script>

			