<?php include('../header-2.php')?>	
  <div class="container main-container">
	 <div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">My Credit</h4>
				<ul class="nav side-nav"> 
				<li><a href="../index.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li> <a href="../my-credit/credit-factors.php"><i class="ion-clipboard"></i> Credit Factors</a> </li>
				 <li> <a href="../my-credit/credit-report.php"><i class="ion-document-text"></i> Credit Report</a></li>
				 <li><a href="../my-credit/dispute-progress.php"><i class="ion-arrow-graph-up-right"></i>Dispute Progress</a></li> 
				 <li class="active"><a href="../my-credit/upload-documents.php"><i class="ion-upload"></i> Upload Documents</a> </li> 
				 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Upload Documents</h3>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  Upload Documents STARTS here ========--->
		
		<section class="upload-documents">
			<div class="col-md-8 col-md-offset-2">
				 <form class="">					 
					<div class="progress documents-progress form-group">
					   <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
							<span class="completed">30% Completed</span>
						  </div>
						</div>
						
						<div class="clearfix"></div>
						
						<div class="form-group box advanced-upload">						
						 <div class="box-input">
						    <i class="ion-upload"></i>
							<div class="clearfix"></div>
							<input class="box__file" type="file" name="files[]" id="file" data-multiple-caption="{count} files selected" multiple />
							<label for="file"><strong>Choose a file</strong>
							<span class="box__dragndrop"> or drag it here</span>.</label>
							<button class="box__button" type="submit">Upload</button>
						  </div>
						  <div class="box__uploading">Uploading&hellip;</div>
						  <div class="box__success">Done!</div>
						  <div class="box__error">Error! <span></span>.</div>
						
						</div>
						
						<div class="form-group">
							<select class="form-control fancy-select">
							   <option>Select file type</option>
							   <option>Social Security Card</option>
							   <option>ID/License</option>
							   <option> Utility Bill</option>		   
						   </select>
						</div>
						<div class="form-group">
						 <button type="button" class="btn btn-primary btn-lg">Upload Documents</button>
						</div>
				 </form>
				 <div class="clearfix"></div><br>
				<!---========= My-documents Starts ========-->
				
				 <div class="my-documents">
				 <h4>My Documents</h4><hr>
				 
				 <div class="col-md-4 col-sm-4 padd_left_0 text-center">
					  <div class="document filled">
					    <a class="delete-doc"><i class="ion-close-circled"></i></a>
					  <a class="example-image-link" href="images/Social-Security-Card.png" data-lightbox="example-1">
					  <img class="example-image" src="../images/Social-Security-Card.png" alt="image-1" /></a>					   
					   
					  </div>
                 <h5 class="document-name">Social Security Card</h5>				  
				 </div>
				 
				 <div class=" col-md-4 col-sm-4 padd_left_0 text-center">
					  <div class="document">
					   <a class="delete-doc"><i class="ion-close-circled"></i></a>
					  No Card </div>
					  <h5 class="document-name">State Issue ID/ License</h5>	
				   </div>
				   <div class="col-md-4 col-sm-4 padd_left_0 text-center">
					  <div class="document">
					   <a class="delete-doc"><i class="ion-close-circled"></i></a>
					  No Bill</div>
					  <h5 class="document-name">Copy Of Utility Bill</h5>	
				   </div>		 

				 </div>
				 <!---========= My-documents END========-->
				 
			</div>
		</section>
       <!----===============   Upload Documents END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include('../footer-2.php')?>	
<script>

	'use strict';

	;( function ( document, window, index )
	{
		// feature detection for drag&drop upload
		var isAdvancedUpload = function()
			{
				var div = document.createElement( 'div' );
				return ( ( 'draggable' in div ) || ( 'ondragstart' in div && 'ondrop' in div ) ) && 'FormData' in window && 'FileReader' in window;
			}();


		// applying the effect for every form
		var forms = document.querySelectorAll( '.box' );
		Array.prototype.forEach.call( forms, function( form )
		{
			var input		 = form.querySelector( 'input[type="file"]' ),
				label		 = form.querySelector( 'label' ),
				errorMsg	 = form.querySelector( '.box__error span' ),
				restart		 = form.querySelectorAll( '.box__restart' ),
				droppedFiles = false,
				showFiles	 = function( files )
				{
					label.textContent = files.length > 1 ? ( input.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', files.length ) : files[ 0 ].name;
				},
				triggerFormSubmit = function()
				{
					var event = document.createEvent( 'HTMLEvents' );
					event.initEvent( 'submit', true, false );
					form.dispatchEvent( event );
				};

			// letting the server side to know we are going to make an Ajax request
			var ajaxFlag = document.createElement( 'input' );
			ajaxFlag.setAttribute( 'type', 'hidden' );
			ajaxFlag.setAttribute( 'name', 'ajax' );
			ajaxFlag.setAttribute( 'value', 1 );
			form.appendChild( ajaxFlag );

			// automatically submit the form on file select
			input.addEventListener( 'change', function( e )
			{
				showFiles( e.target.files );

				
				triggerFormSubmit();

				
			});

			// drag&drop files if the feature is available
			if( isAdvancedUpload )
			{
				form.classList.add( 'has-advanced-upload' ); // letting the CSS part to know drag&drop is supported by the browser

				[ 'drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop' ].forEach( function( event )
				{
					form.addEventListener( event, function( e )
					{
						// preventing the unwanted behaviours
						e.preventDefault();
						e.stopPropagation();
					});
				});
				[ 'dragover', 'dragenter' ].forEach( function( event )
				{
					form.addEventListener( event, function()
					{
						form.classList.add( 'is-dragover' );
					});
				});
				[ 'dragleave', 'dragend', 'drop' ].forEach( function( event )
				{
					form.addEventListener( event, function()
					{
						form.classList.remove( 'is-dragover' );
					});
				});
				form.addEventListener( 'drop', function( e )
				{
					droppedFiles = e.dataTransfer.files; // the files that were dropped
					showFiles( droppedFiles );

					
					triggerFormSubmit();

									});
			}


			// if the form was submitted
			form.addEventListener( 'submit', function( e )
			{
				// preventing the duplicate submissions if the current one is in progress
				if( form.classList.contains( 'is-uploading' ) ) return false;

				form.classList.add( 'is-uploading' );
				form.classList.remove( 'is-error' );

				if( isAdvancedUpload ) // ajax file upload for modern browsers
				{
					e.preventDefault();

					// gathering the form data
					var ajaxData = new FormData( form );
					if( droppedFiles )
					{
						Array.prototype.forEach.call( droppedFiles, function( file )
						{
							ajaxData.append( input.getAttribute( 'name' ), file );
						});
					}

					// ajax request
					var ajax = new XMLHttpRequest();
					ajax.open( form.getAttribute( 'method' ), form.getAttribute( 'action' ), true );

					ajax.onload = function()
					{
						form.classList.remove( 'is-uploading' );
						if( ajax.status >= 200 && ajax.status < 400 )
						{
							var data = JSON.parse( ajax.responseText );
							form.classList.add( data.success == true ? 'is-success' : 'is-error' );
							if( !data.success ) errorMsg.textContent = data.error;
						}
						else alert( 'Error. Please, contact the webmaster!' );
					};

					ajax.onerror = function()
					{
						form.classList.remove( 'is-uploading' );
						alert( 'Error. Please, try again!' );
					};

					ajax.send( ajaxData );
				}
				else // fallback Ajax solution upload for older browsers
				{
					var iframeName	= 'uploadiframe' + new Date().getTime(),
						iframe		= document.createElement( 'iframe' );

						$iframe		= $( '<iframe name="' + iframeName + '" style="display: none;"></iframe>' );

					iframe.setAttribute( 'name', iframeName );
					iframe.style.display = 'none';

					document.body.appendChild( iframe );
					form.setAttribute( 'target', iframeName );

					iframe.addEventListener( 'load', function()
					{
						var data = JSON.parse( iframe.contentDocument.body.innerHTML );
						form.classList.remove( 'is-uploading' )
						form.classList.add( data.success == true ? 'is-success' : 'is-error' )
						form.removeAttribute( 'target' );
						if( !data.success ) errorMsg.textContent = data.error;
						iframe.parentNode.removeChild( iframe );
					});
				}
			});


			// restart the form if has a state of error/success
			Array.prototype.forEach.call( restart, function( entry )
			{
				entry.addEventListener( 'click', function( e )
				{
					e.preventDefault();
					form.classList.remove( 'is-error', 'is-success' );
					input.click();
				});
			});

			// Firefox focus bug fix for file input
			input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
			input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });

		});
	}( document, window, 0 ));

	
</script>