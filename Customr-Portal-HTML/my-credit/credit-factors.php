<?php include('../header-2.php')?>	
   
   	<div class="container main-container">
	<div class="row">
	
    <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
	  <h4 class="sidebar-title">My Credit</h4>
				<ul class="nav side-nav"> 
				<li><a href="../index.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li class="active"> <a href="../my-credit/credit-factors.php"><i class="ion-clipboard"></i> Credit Factors</a> </li>
				 <li> <a href="../my-credit/credit-report.php"><i class="ion-document-text"></i> Credit Report</a></li>
				 <li><a href="../my-credit/dispute-progress.php"><i class="ion-arrow-graph-up-right"></i>Dispute Progress</a></li> 
				 <li><a href="../my-credit/upload-documents.php"><i class="ion-upload"></i> Upload Documents</a> </li> 
				 </ul>  
				</div>
   
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Credit Factors</h3>
		<form class="pull-right form-inline">
		 <div class="form-group">
		   <select class="form-control fancy-select">
		   <option>Select by date</option>
		   <option>Monthly</option>
		   <option>yearly</option>
		   <option>Select by date</option>		   
		   </select>		     
		   </div>
		 <button type="button" class="btn btn-default btn-print"><i class="ion-printer"></i></button>
		 </form>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  score-meter STARTS here ========--->
		<section class="score-meter factors-score-meter">
			<div class="meter">
			<div class="score-count col-md-3 col-sm-12">760</div>
			<div class="provided-by col-md-3 col-sm-12">
			<span>Provided by: </span> 
			<img src="../images/transu-large.png">
			</div>
		
		<div class="col-md-6"> 
			<div class="score-rating">Credit Rating : <strong>Excellent</strong></div>
			 <div class="progress-container">
			  <div class="progress-bar" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style="width: 82%;"></div>
			  <div class="clearfix"></div>
		 <div class="progress meter-progress">
			 <span class="poor"></span>
			 <span class="fair"></span>
			 <span class="average"></span>
			 <span class="good"></span>
			 <span class="excellent"></span>
		 </div>
		  <div class="clearfix"></div>
		 <ul class="nav progress-counting">
			 <li>300</li>
			 <li>580</li>
			 <li>640</li>
			 <li>700</li>
			 <li>750</li>
			 <li>850</li>
		  </ul>
		  	<p class="updation text-center"><i class="ion-clock"></i> Updated June 21,2016 &nbsp;|&nbsp; Next Update in 6 days</p>
          </div><!---- progress-container-->
          </div>
             		  
	
		
		</div><!---- meter Close here-->

		</section>
		
<!----============  score-meter END here ===========--->
	
		<div class="clearfix"></div>
		
<!----============ Factors Start  Here ===========--->	
	
			<div class="section-title">
			<h3>What can impact your score? </h3>
			</div>
		
		<section class="factors">		
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		
    <!----==== Factor-1 ====--->	
  <div class="panel panel-default factor">
    <div class="panel-heading" role="tab" id="headingOne">
	<div class="col-md-5 col-sm-6 factor-des">
      <h4 class="panel-title">Credit Card Utilization <span>
	    <img src="../images/meter-high.png">High Impact</span></h4>      		
         <p>The amount of your total available Credit that you are using </p>
    </div>
	<div class="col-md-5 col-sm-4 factor-score text-center">		
		<p class="factor-rating excellent">
		<span class="factor-percentage">5%</span>
		 <strong>Excellent</strong>
		</p>
	 </div>
	<div class="col-md-2 col-sm-2 text-center">
	<a data-toggle="collapse" data-parent="#accordion" href="#factorOne" aria-expanded="true" aria-controls="factorOne" class="collapsed btn btn-primary-outline view-detail"></a> 
	 </div>
	<div class="clearfix"></div>
	</div>
	
    <div id="factorOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
	  <center>
       <h4>Where you stand</h4>
	   <p>You are using 1% of your available credit . Thats the same as your last update.</p>
	   
	   <div class="your-score">
		   <p class="factor-rating excellent">
			<span class="factor-percentage">75%+</span>
			 <strong>Very Poor</strong>
			</p>
			
			<p class="factor-rating poor">
			<span class="factor-percentage">50-74%+</span>
			 <strong>Poor</strong>
			</p>
			
			<p class="factor-rating fair">
			<span class="factor-percentage">30-49% </span>
			 <strong>Fair</strong>
			</p>
			
			<p class="factor-rating good">
			<span class="factor-percentage">10-29%</span>
			 <strong>Good</strong>
			</p>
			
			<p class="factor-rating excellent active">
			<span class="factor-percentage">0-9%</span>
			 <strong>Excellent</strong>
			</p>
			
	     </div>	   
	   </center>
	     <div class="clearfix"></div>
	   <div class="col-md-6">
		   <h4>How it's Calculated</h4>
		   <table class="table-bordered table">
		   <tr>
			   <td>Your total credit card balance </td>
			   <td align="right">$400</td>
		   </tr>
		   <tr>
			   <td>Your total credit card  limit </td>
			   <td align="right">$10000</td>
		   </tr>
		   <tr>
			   <td colspan="2" align="center"><strong> $400 ÷ $10000 = 4%</strong></td>
		   </tr>
			</table>
	    </div>
		
		 <div class="col-md-6">
	   <h4>Usage by Account</h4>
	   
	   <div class="account-usage">
		   <a href="#"> Loream ipsum</a>
			 <span class="pull-right">50%</span>
		     <div class="clearfix"></div>
		      <div class="progress">
			   <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div></div>
			    <p><span>Balance: $500  &nbsp;   Reported : 5 Apr, 2016  </span> 
			     <span class="pull-right">$200 Limit</span></p>
	     </div>   
		 
		<div class="account-usage">
		   <a href="#"> Loream ipsum</a>
			 <span class="pull-right">50%</span>
		     <div class="clearfix"></div>
		      <div class="progress">
			   <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:30%;"></div></div>
			    <p><span>Balance: $500  &nbsp;   Reported : 5 Apr, 2016  </span> 
			     <span class="pull-right">$200 Limit</span></p>
	     </div> 
		 
		 <div class="account-usage">
		   <a href="#"> Loream ipsum</a>
			 <span class="pull-right">50%</span>
		     <div class="clearfix"></div>
		      <div class="progress">
			   <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"></div></div>
			    <p><span>Balance: $500  &nbsp;   Reported : 5 Apr, 2016  </span> 
			     <span class="pull-right">$200 Limit</span></p>
	     </div> 
	
	
	    </div>
		
		
		
      </div>
    </div>
  </div>
   <!----==== Factor-1 END ====--->
   
    <!----==== Factor-2 ====--->	
  <div class="panel panel-default factor">
    <div class="panel-heading" role="tab" id="headingTwo">
	<div class="col-md-5 col-sm-6 factor-des">
      <h4 class="panel-title">Payment History <span>
	    <img src="../images/meter-high.png">High Impact</span></h4>      		
         <p>The amount of your total available Credit that you are using </p>
    </div>
	<div class="col-md-5 col-sm-4 factor-score text-center">		
		<p class="factor-rating good">
		<span class="factor-percentage">99.3%</span>
		 <strong>Good</strong>
		</p>		
	 </div>
	 
	 
	<div class="col-md-2 col-sm-2 text-center">
	<a data-toggle="collapse" data-parent="#accordion" href="#factorTwo" aria-expanded="true" aria-controls="factorTwo" class="collapsed btn btn-primary-outline view-detail"></a> 
	 </div>
	<div class="clearfix"></div>
	</div>
	
    <div id="factorTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
	  <center>
       <h4>Where you stand</h4>
	   <p>You have been on the time with  99.3% of your payment. Thats the same as your last update.</p>
	   
	   <div class="your-score">
	   
	      <p class="factor-rating very-poor">
			<span class="factor-percentage">< 97%+</span>
			 <strong>Very Poor</strong>
			</p>
			
			 <p class="factor-rating poor">
			<span class="factor-percentage">97%</span>
			 <strong>Poor</strong>
			</p>
			
			<p class="factor-rating fair">
			<span class="factor-percentage">98% </span>
			 <strong>Fair</strong>
			</p>
			
			<p class="factor-rating good active">
			<span class="factor-percentage">99% </span>
			 <strong>Good</strong>
			</p>
			
			<p class="factor-rating excellent">
			<span class="factor-percentage">100% </span>
			 <strong>Excellent</strong>
			</p>
		  
	     </div>	   
	   </center>
	     <div class="clearfix"></div>
		 
		  <div class="col-md-6">
			   <h4>How it's Calculated</h4>
			   <table class="table-bordered table">
			   <tr>
				   <td>Your on time payments  </td>
				   <td align="right">148</td>
			   </tr>
			   <tr>
				   <td>Your total payments </td>
				   <td align="right">149</td>
			   </tr>
			    <tr>
				   <td align="center"><strong>148 &divide; 149</strong></td>
				   <td align="right"><strong>99.3%</strong></td>
			   </tr>
			  
				</table>
	    </div>
		
	   <div class="col-md-6">
			   <h4>Missed Payments</h4>
			   <table class="table-bordered table">
			   <tr>
				   <td>DSNB MACYS  </td>
				   <td align="right">1</td>
			   </tr>
			   <tr>
				   <td>Loream Ipsum</td>
				   <td align="right">&nbsp;</td>
			   </tr>
			    <tr>
				   <td><strong>Total missed payments</strong></td>
				   <td align="right"><strong>1</strong></td>
			   </tr>
				</table>
	    </div>
		
      </div>
    </div>
  </div>
<!----==== Factor-2 END ====--->
   
<!----==== Factor-3 Start ====--->	
  <div class="panel panel-default factor">
    <div class="panel-heading" role="tab" id="headingThree">
	<div class="col-md-5 col-sm-6 factor-des">
      <h4 class="panel-title">Derogatory Marks <span>
	    <img src="../images/meter-high.png">High Impact</span></h4>      		
         <p>The amount of your total available Credit that you are using </p>
    </div>
	<div class="col-md-5 col-sm-4 factor-score text-center">		
		<p class="factor-rating excellent">
		<span class="factor-percentage">0</span>
		 <strong>Excellent</strong>
		</p>
				
	 </div>
	<div class="col-md-2 col-sm-2 text-center">
	<a data-toggle="collapse" data-parent="#accordion" href="#factorThree" aria-expanded="true" aria-controls="factorThree" class="collapsed btn btn-primary-outline view-detail"></a> 
	 </div>
	<div class="clearfix"></div>
	</div>
	
    <div id="factorThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
	  <center>
       <h4>Where you stand</h4>
	   <p>You have 0 derogaatory marks. Thats the same as your last update.</p>
	   
	   <div class="your-score">
	   
	   <p class="factor-rating very-poor">
			<span class="factor-percentage">4+ </span>
			 <strong>Very Poor</strong>
			</p>
			
	    <p class="factor-rating poor">
			<span class="factor-percentage">2-3</span>
			 <strong>Poor</strong>
			</p>
			
		<p class="factor-rating fair">
			<span class="factor-percentage">1</span>
			 <strong>Fair</strong>
			</p>
			
			<p class="factor-rating good">
			<span class="factor-percentage">0</span>
			 <strong>Good</strong>
			</p>	
			
		<p class="factor-rating excellent active">
			<span class="factor-percentage">0</span>
			 <strong>Excellent</strong>
			</p>
	 
	     </div>	   
	   </center>
	     <div class="clearfix"></div>
		 
		  <div class="col-md-6">
			   <h4>How it's Calculated</h4>
			   <table class="table-bordered table">
			   <tr>
				   <td>Open accounts in collections</td>
				   <td align="right">0</td>
			   </tr>
			   <tr>
				   <td>Nagative public records  </td>
				   <td align="right">0</td>
			   </tr>
			    <tr>
				   <td><strong>Total Derogatory Marks </strong></td>
				   <td align="right"><strong>0</strong></td>
			   </tr>
			  
				</table>
	    </div>
		
	   <div class="col-md-6">
	   <h4>Derogatory  Account</h4>
	   
	   <div class="account-usage">
		   <a href="#"> Loream ipsum</a>
			 <span class="pull-right">50%</span>
		     <div class="clearfix"></div>
		      <div class="progress">
			   <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div></div>
			    <p><span>Balance: $500  &nbsp;   Reported : 5 Apr, 2016  </span> 
			     <span class="pull-right">$200 Limit</span></p>
	     </div>   
		 
		<div class="account-usage">
		   <a href="#"> Loream ipsum</a>
			 <span class="pull-right">50%</span>
		     <div class="clearfix"></div>
		      <div class="progress">
			   <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:30%;"></div></div>
			    <p><span>Balance: $500  &nbsp;   Reported : 5 Apr, 2016  </span> 
			     <span class="pull-right">$200 Limit</span></p>
	     </div> 
		 
		 <div class="account-usage">
		   <a href="#"> Loream ipsum</a>
			 <span class="pull-right">50%</span>
		     <div class="clearfix"></div>
		      <div class="progress">
			   <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"></div></div>
			    <p><span>Balance: $500  &nbsp;   Reported : 5 Apr, 2016  </span> 
			     <span class="pull-right">$200 Limit</span></p>
	     </div> 
	
	
	    </div>
		
      </div>
    </div>
  </div>
   <!----==== Factor-3 END ====--->
   
   
    <!----==== Factor-4 ====--->	
  <div class="panel panel-default factor">
    <div class="panel-heading" role="tab" id="headingFour">
	<div class="col-md-5 col-sm-6 factor-des">
      <h4 class="panel-title">Age of Credit History<span>
	    <img src="../images/meter-medium.png">Medium Impact</span></h4>      		
         <p>The amount of your total available Credit that you are using </p>
    </div>
	<div class="col-md-5 col-sm-4 factor-score text-center">		
		<p class="factor-rating fair">
		<span class="factor-percentage">6 Yrs 1M</span>
		 <strong>Fair</strong>
		</p>		
	 </div>
	<div class="col-md-2 col-sm-2 text-center">
	<a data-toggle="collapse" data-parent="#accordion" href="#factorFour" aria-expanded="true" aria-controls="factorFour" class="collapsed btn btn-primary-outline view-detail"></a> 
	 </div>
	<div class="clearfix"></div>
	</div>
	
    <div id="factorFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">
	  <center>
       <h4>Where you stand</h4>
	   <p>You average age of  credit history by averaging the ages of your open credit accounts.</p>
	   
	   <div class="your-score">
	   
	   
	   <p class="factor-rating very-poor">
			<span class="factor-percentage"> < 2yrs </span>
			 <strong>Very Poor</strong>
			</p> 
      <p class="factor-rating poor">
			<span class="factor-percentage"> 2-4yrs </span>
			 <strong>Poor</strong>
			</p>	

    <p class="factor-rating fair active">
			<span class="factor-percentage"> 5-6yrs </span>
			 <strong>Fair</strong>
			</p>

    <p class="factor-rating  good">
			<span class="factor-percentage">7-8yrs</span>
			 <strong>Good</strong>
			</p>

   <p class="factor-rating excellent ">
			<span class="factor-percentage">9+yrs</span>
			 <strong>Excellent</strong>
			</p>				
			 
	     </div>	   
	   </center>
	     <div class="clearfix"></div>
		 
		  <div class="col-md-6">
			   <h4>How it's Calculated</h4>
			   <table class="table-bordered table">
			   <tr>
				   <td>Total years of accounts  </td>
				   <td align="right">18</td>
			   </tr>
			   <tr>
				   <td>Total open accounts</td>
				   <td align="right">3</td>
			   </tr>
			    <tr>
				<td align="center" colspan="2"><strong>18 &divide; 3 = 6</strong></td>
			   </tr>
			  
				</table>
	    </div>
		
	   <div class="col-md-6">
			   <h4>Age of Your Accounts</h4>
			   <table class="table-bordered table">
			   <tr>
				   <td><strong>DSNB </strong></td>
				   <td align="right">8 Years, 4 Months</td>
			   </tr>
			   <tr>
				   <td><strong>FST PRMIER</strong></td>
				   <td align="right">6 Years, 4 Months</td>
			   </tr>
			    <tr>
				   <td><strong>CAPITAL ONE</strong></td>
				   <td align="right">3 Years, 4 Months</td>
			   </tr>
			    <tr>
				   <td><strong>Average age of credit</strong></td>
				   <td align="right"><strong>6 Years</strong></td>
			   </tr>
				</table>
	    </div>
		
      </div>
    </div>
  </div>
<!----==== Factor-4 END ====--->

 <!----==== Factor-5 ====--->	
  <div class="panel panel-default factor">
    <div class="panel-heading" role="tab" id="headingFive">
	<div class="col-md-5 col-sm-6 factor-des">
      <h4 class="panel-title">Total Accounts<span>
	    <img src="../images/meter-low.png">Low Impact</span></h4>      		
         <p>The amount of your total available Credit that you are using </p>
    </div>
	<div class="col-md-5 col-sm-4 factor-score text-center">		
		<p class="factor-rating poor">
		<span class="factor-percentage">3</span>
		 <strong>poor</strong>
		</p>
	 </div>
	<div class="col-md-2 col-sm-2 text-center">
	<a data-toggle="collapse" data-parent="#accordion" href="#factorFive" aria-expanded="true" aria-controls="factorFive" class="collapsed btn btn-primary-outline view-detail"></a> 
	 </div>
	<div class="clearfix"></div>
	</div>
	
    <div id="factorFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">
	  <center>
       <h4>Where you stand</h4>
	   <p>You have <strong>3</strong> total accounts. That's the same as your last update.</p>
	   
	   <div class="your-score">
	   
	    <p class="factor-rating very-poor">
			<span class="factor-percentage">0</span>
			 <strong>Very Poor</strong>
			</p>	
			
		<p class="factor-rating poor active">
			<span class="factor-percentage">1-3</span>
			 <strong>Poor</strong>
			</p>
			
			
		<p class="factor-rating fair">
			<span class="factor-percentage">4-6</span>
			 <strong>Fair</strong>
			</p>
			
		<p class="factor-rating good">
			<span class="factor-percentage">7-10</span>
			 <strong>Good</strong>
			</p>
		
		<p class="factor-rating excellent ">
			<span class="factor-percentage">11+</span>
			 <strong>Excellent</strong>
			</p>
			  
	     </div>	   
	   </center>
	     <div class="clearfix"></div>
		 
		  <div class="col-md-6">
			   <h4>How it's Calculated</h4>
			    <table class="table-bordered table">
			   <tr>
				   <td>Total open accounts</td>
				   <td align="right">2</td>
			   </tr>
			   <tr>
				   <td>Total closed accounts</td>
				   <td align="right">1</td>
			   </tr>
			    <tr>
				<td align="center" colspan="2"><strong>2 + 1 = 3</strong></td>
			   </tr>
			  
				</table>
	    </div>
		
	   <div class="col-md-6">
			   <h4>Your Accounts</h4>
			   <table class="table-bordered table">
			   <tr>
				   <td><i class="ion-model-s"></i> <strong>Tyota MTR </strong></td>
				   <td>Open</td>
				   <td align="right"><small>Reported on 18 Aug 2016</small></td>
			   </tr>
			   <tr>
				   <td><i class="ion-card"></i>  <strong>Capital One</strong></td>
				   <td>Open</td>
				   <td align="right"><small>Reported on  25 Nov 2016</small></td>
			   </tr>
			    <tr>
				   <td><i class="ion-card"></i>  <strong>Capital One</strong></td>
				   <td>Closed</td>
				   <td align="right"><small>Reported on  18 Apr 2016</small></td>
			   </tr>
			    
				</table>
	    </div>
		
      </div>
    </div>
  </div>
<!----==== Factor-5 END ====--->

<!----==== Factor-6 ====--->	
  <div class="panel panel-default factor">
    <div class="panel-heading" role="tab" id="headingSix">
	<div class="col-md-5 col-sm-6 factor-des">
      <h4 class="panel-title">Credit Inquiries<span>
	    <img src="../images/meter-low.png">Low Impact</span></h4>      		
         <p>The amount of your total available Credit that you are using </p>
    </div>
	<div class="col-md-5 col-sm-4 factor-score text-center">		
		<p class="factor-rating fair">
		<span class="factor-percentage">3</span>
		 <strong>fair</strong>
		</p>		
	 </div>
	<div class="col-md-2 col-sm-2 text-center">
	<a data-toggle="collapse" data-parent="#accordion" href="#factorSix" aria-expanded="true" aria-controls="factorSix" class="collapsed btn btn-primary-outline view-detail"></a> 
	 </div>
	<div class="clearfix"></div>
	</div>
	
    <div id="factorSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
      <div class="panel-body">
	  <center>
       <h4>Where you stand</h4>
	   <p>You have  <strong>3</strong> inquiry  on your credit report . That's the same as your last update.</p>
	   
	   <div class="your-score">
	   <p class="factor-rating very-poor">
			<span class="factor-percentage">9+</span>
			 <strong>Very Poor</strong>
			</p>
			
		<p class="factor-rating poor">
			<span class="factor-percentage">5-8</span>
			 <strong>Poor</strong>
			</p>
			
	<p class="factor-rating fair active">
			<span class="factor-percentage">3-4</span>
			 <strong>Fair</strong>
			</p>
			
	<p class="factor-rating good">
			<span class="factor-percentage">1-2 </span>
			 <strong>Good</strong>
			</p>
			
	<p class="factor-rating excellent ">
			<span class="factor-percentage">0 </span>
			 <strong>Good</strong>
			</p>

	     </div>	   
	   </center>
	     <div class="clearfix"></div>
		 
		  <div class="col-md-6">
			   <h4>How it's Calculated</h4>
			    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
					Audax negotium, dicerem impudens, nisi hoc institutum 
					postea translatum ad philosophos nostros esset.
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
					Audax negotium, dicerem impudens, nisi hoc institutum 
					postea translatum ad philosophos nostros esset.</p>
	    </div>
		
	   <div class="col-md-6">
			   <h4>Credit Inquiries</h4>
			   <table class="table-bordered table">
			   <tr>
				   <td></i>T-Mobile</td>
				   <td align="right"><small>Inquiry on 15 oct, 2012 </small></td>
			   </tr>
			   <tr>
				   <td>Loream Ipsum</td>
				   <td align="right"><small>Inquiry on 20 May, 2012</small></td>
			   </tr>
			   <tr>
				   <td></i>T-Mobile</td>
				   <td align="right"><small>Inquiry on 15 oct, 2012 </small></td>
			   </tr>
			    <tr>
				   <td><strong>Total Inquiries</strong> </td>
				   <td align="right"><strong>3</strong></td>
			   </tr>
			    
				</table>
	    </div>
		
      </div>
    </div>
  </div>
<!----==== Factor-6 END ====--->

   

   
   
 </div><!---=== panel-group ==-->

		</section>
<!----===========  content-container END here ======--->	

		</div><!----===========  content-container END here ======--->	

   </div><!----  Row END here --->
 </div><!----  Container END here --->
 
<?php include('../footer-2.php')?>	