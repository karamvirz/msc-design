<?php include('../header-2.php')?>	
	<div class="container main-container">
	 <div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">My Credit</h4>
				<ul class="nav side-nav"> 
				<li><a href="../index.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li> <a href="../my-credit/credit-factors.php"><i class="ion-clipboard"></i> Credit Factors</a> </li>
				 <li  class="active"> <a href="../my-credit/credit-report.php"><i class="ion-document-text"></i> Credit Report</a></li>
				 <li><a href="../my-credit/dispute-progress.php"><i class="ion-arrow-graph-up-right"></i>Dispute Progress</a></li> 
				 <li><a href="../my-credit/upload-documents.php"><i class="ion-upload"></i> Upload Documents</a> </li> 
				 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Credit Report</h3>
		
		<form class="pull-right form-inline">
		 
		 <a class="btn btn-primary btn-sm" href="dispute-inaccuracies.php">
		 <i class="ion-qr-scanner"></i> Dispute Inaccuracies</a>
		 <button class="btn btn-default btn-sm" type="button"><i class="ion-android-download"></i> Download Report</button>
		 </form>
		
		</div>
		<div class="clearfix"></div>
		
		<!----==========  credit-report STARTS here ========--->
		
		<section class="credit-report">
		 
				 <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#overview" data-toggle="tab">overview</a></li>
			<li><a href="#personal-information" data-toggle="tab">
			 Personal Information</a></li>
			<li><a href="#accounts" data-toggle="tab">
			<span class="label label-default">7</span> Positive Accounts</a></li>
			
			<li><a href="#derogatory" data-toggle="tab">
			<span class="label label-default">0</span>  Derogatory Accounts</a></li>
		
			<li>
			<a href="#credit-inquiries" data-toggle="tab">
			<span class="label label-default">1</span> Inquiries </a></li>
			
		  </ul>

		  <!-- Tab-content -->
		  <div class="tab-content">
		  
		  <!----=========== overview END ============--->
			<div class="tab-pane active" id="overview">
			
			<?php include 'credit-report-overview.php'?>
					 
			</div>
           <!----=========== overview END ============--->

         <div class="tab-pane" id="accounts">	
		 
			<?php include 'credit-report-accounts.php'?>	
			
		     </div>
           <!----============ Accounts END ===========--->
		   
			<div class="tab-pane" id="credit-inquiries">
			
			<?php include 'credit-report-inquiries.php'?>
			
			  </div>
			   <!----============ credit-inquiries END ===========--->
			   
		
			<div class="tab-pane" id="derogatory">			
			<?php include 'credit-report-derogatory.php'?>
			  </div>
			 <!----============ credit-collections END ===========--->  
			 
			<div class="tab-pane" id="personal-information">	
			
			<?php include 'personal-information.php'?>
			
			 </div>
			  <!----============ public-records END ===========---> 
			  
		  </div>	   
		
		</section>
		<!----============  credit-report END here ===========--->
		
		
		
</div>
<!----===========  content-container END here ======--->	

</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include('../footer-2.php')?>