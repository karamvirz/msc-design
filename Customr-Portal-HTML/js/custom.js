$(function() {
    $('.navbar-nav > li.dropdown > a').click(function() {
        $(this).next('.dropdown-menu').slideToggle(300);
    });
});

// navbar-toggle
$(function() {
	$('.navbar-toggle').click(function() {
    //$('.navbar-toggle').click(function() {
        $(this).toggleClass('closed');
        $('.mobile-menu-sidebar').toggleClass('active');
        $('#main-navbartop').toggleClass('active');
		$('.main_wrapper').toggleClass('active');
		$('html').toggleClass('active');
    });
}); 

$(function() {
    $('.toggle-close').click(function() {
	$('.navbar-toggle').toggleClass('closed');
    });
}); 


// Sticky nav
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if(scroll >= 80) {
        $(".main-navbar").addClass("fixed");
        $(".sidebar").addClass("fixed");
        $(".content-container").addClass("fixed-side");
    } else {
        $(".main-navbar").removeClass("fixed");
		$(".sidebar").removeClass("fixed");
		$(".content-container").removeClass("fixed-side");
    }
	
	 if(scroll >= 280) {
        $(".account-pages .sidebar").addClass("fixed");
        $(".account-pages .content-container").addClass("fixed-side");
	 }
	 else {
        $(".account-pages .sidebar").removeClass("fixed");
		 $(".account-pages .content-container").removeClass("fixed-side");
	 }
	
	
});

//
$(function() {
$(".view-detail").click(function(){
$(this).parent().parent().toggleClass('active');
});
});
//
 $(function () {
$('[data-toggle="popover"]').popover();
$('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
})
//
