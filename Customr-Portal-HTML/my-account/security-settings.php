<?php include '../header-2.php'?>

<div class="clearfix"></div>
	
  <div class="container main-container">
	 <div class="row">
          <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		    <h4 class="sidebar-title">My Account</h4>
				<ul class="nav side-nav"> 
				 <li ><a href="../my-account/personal-profile.php"><i class="ion-ios-person-outline"></i> Personal Profile </a> </li>
				 <li class="active"><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings </a> </li> 	 
				  <li><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li>
				  <li><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication </a> </li> 
				  
				  <li class="divider"></li>				  
				  <li><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			      <li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
				   
			 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<form class="form-horizontal">		
			<div class="section-title">
			<h3>Change Email</h3>
			</div>
			<div class="clearfix"></div>
		
		
		<section class="edit-profile security-settings">

		  <div class="form-group">
			<label class="col-md-3 col-sm-3 control-label">Currrent Email :</label>
			   <div class="col-md-9 col-sm-9">			
					<p class="current-email">johnSmith@domainname.com </p> 
					  	</div>
					  	</div>
				
				   <div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">New Email :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="email" class="form-control" >
						  </div>
					  </div>
					  
					<div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">Re-Enter New Email :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="email" class="form-control">
						  </div>
					  </div>
					  
					  <div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">Verify current password :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="password" class="form-control">
						  </div>
					  </div>
					  
					  <div class="form-group">					  
						 <div class="col-md-5 col-sm-9 col-md-offset-3 col-sm-offset-3">	
						  <button type="submit" class="btn btn-primary btn-md">Save Email</button>
						  </div>
					  </div>

               </section>					  
				 
			<div class="clearfix"></div>
			
			<div class="section-title">
			  <h3>Change Password</h3>
			</div>
			
			
				<section class="edit-profile security-settings">		 
				
				   <div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">New Password :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="password" class="form-control" >
						  </div>
					  </div>
					  
					<div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">Re-Enter New Password :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="password" class="form-control">
						  </div>
					  </div>
					  
					  <div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">Verify current password :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="password" class="form-control">
						  </div>
					  </div>
					  
					   <div class="form-group">					  
						  <div class="col-md-5 col-sm-9 col-md-offset-3 col-sm-offset-3">	
						  <button type="submit" class="btn btn-primary btn-md">Save Password</button>
						  </div>
					  </div>

               </section>


   <div class="clearfix"></div>
			
			<div class="section-title">
			  <h3>Change Security Question</h3>
			</div>
			
			
				<section class="edit-profile security-settings">			 			
				
				   <div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">New Security Question :</label>
						 <div class="col-md-5 col-sm-9">	
						  <select class="fancy-select form-control">
						   <option>Please Select a question</option>
						   <option>Question</option>
						   <option>Question</option>
						   <option>Question</option>
						   <option>Question</option>						  
						   </select>
						  </div>
					  </div>
					  
					<div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">Answer to Question :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="text" class="form-control">
						  </div>
					  </div>
					  
					  <div class="form-group">
					   <label class="col-md-3 col-sm-3 control-label">Verify current password :</label>
						 <div class="col-md-5 col-sm-9">	
						 <input type="password" class="form-control">
						  </div>
					  </div>
					  
					   <div class="form-group">					  
						 <div class="col-md-5 col-sm-9 col-md-offset-3 col-sm-offset-3">	
						  <button type="submit" class="btn btn-primary btn-md">Save Question</button>
						  </div>
					  </div>

               </section>			   
					 					
				 
				 
				 


         
     
	       </form>
		
       <!----===============   profile END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include '../footer-2.php'?>