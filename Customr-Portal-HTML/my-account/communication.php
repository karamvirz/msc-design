<?php include '../header-2.php'?>
	
  <div class="container main-container">
	 <div class="row">
          <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		    <h4 class="sidebar-title">My Account</h4>
				<ul class="nav side-nav"> 
				 <li ><a href="../my-account/personal-profile.php"><i class="ion-ios-person-outline"></i> Personal Profile </a> </li>
				 <li><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings </a> </li> 	 
				  <li><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li>
				  <li class="active"><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication </a> </li> 
				  
				  <li class="divider"></li>				  
				  <li><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			      <li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
				   
			 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
	
			<div class="section-title">
			<h3> Email Communication</h3>
			</div>
			<div class="clearfix"></div>
		
		
		<section class="communication">
		    <form class="form-horizontal">
			<label class="col-md-4 col-sm-4">Currrent Email :</label>
			   <div class="col-md-8 col-sm-8">			
					<p class="current-email">johnSmith@domainname.com 
					<a href="security-settings.php" class="btn btn-sm btn-success-outline" title="Change Email">
					  <i class="ion-edit"></i> Edit</a></p> 
					  	</div>
						
				<div class="clearfix"></div>
						
				<label class="col-md-4 col-sm-4">Email me important notifications :</label>
			   <div class="col-md-8 col-sm-8">	
				 <div class="radio fancy_radio col-md-3 col-sm-6  padd_left_0">
				 <label><input type="radio" name="status" value="on" checked> <span>ON</span></label> </div>
				 
				 <div class="radio fancy_radio col-md-3 col-sm-6">
				 <label><input type="radio" name="status" value="off"><span> OFF</span></label> </div>
			   
					
					  	</div>
						 
			<div class="clearfix"></div><br>	 			
         <div class="communication-table">		 
             <div class="row">
				 <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				</div>
				 <div class="col-sm-11 col-xs-10">
				 <td><h5>Credit Monitoring</h5>
				 <small>Monitor my credit report and notify me of changes</small></td>				 		 
			      </div>
				  
			    </div>
				<!----ROW END-->
				
				 <div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Bill Reminders</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				<div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Bill Payments</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				<div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Large Purchases</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				<div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Low Balance</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				<div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Low Available Credit</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				<div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Bank Deposits</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				<div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Credit Monitoring</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				<div class="row">
				  <div class="col-sm-1 col-xs-1">
					 <div class="checkbox fancy_checkbox">
					  <label><input type="checkbox"><span></span></label>
					  </div>
				  </div>
				 <div class="col-sm-11 col-xs-10">
				 <h5>Dispute Activity</h5>
				 <small>Monitor my credit report and notify me of changes</small></div>				 		 
			    </div>
				<!----ROW END-->
				
				
 
				 </div>
				 </div>
				  </form>
               </section>					  
				 
			<div class="clearfix"></div>

<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include '../footer-2.php'?>