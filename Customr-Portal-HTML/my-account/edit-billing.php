<?php include '../header-2.php'?>

<div class="clearfix"></div>
	
  <div class="container main-container">
	 <div class="row">
          <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		    <h4 class="sidebar-title">My Account</h4>
				<ul class="nav side-nav"> 
				 <li class="active"><a href="../my-account/personal-profile.php"><i class="ion-ios-person-outline"></i> Personal Profile </a> </li>
				 <li><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings </a> </li> 	 
				  <li><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li>
				  <li><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication </a> </li> 
				  
				  <li class="divider"></li>				  
				  <li><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			      <li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
				   
			 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Edit Billing Information</h3>
		<form class="pull-right form-inline">
		<a href="subscription.php" class="btn btn-sm btn-default">
					  <i class="ion-close-circled"></i> Cancel</a></form>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  billing  STARTS here ========--->
		
		<section class="billing">

        <div class="col-md-8 col-md-offset-2">
				 <form class="form-horizontal">		 
                  
				  <div class="form-group">
				    <label>Billing Information*</label>
				     <div class="row">
                     <div class="col-md-6 col-sm-6">
						<input type="text" class="form-control" placeholder="Name on Card">
						</div>
						<div class="col-md-6 col-sm-6 padd_left_0">
						<input type="text" class="form-control" placeholder="Card Number">
						</div>			   	  
						</div>						
						</div>
					<div class="clearfix"></div>
					
					
					 <div class="form-group">					
					     <input type="text" class="form-control" placeholder="Billing Address">
							</div>				

					 <div class="form-group">
						 <div class="row">	
                      <div class="col-md-5 col-sm-5">
							<select class="form-control fancy-select">
							   <option> Card Type</option>
							   <option>Card Type</option>
							   <option>Card Type</option>
							   <option> Card Type</option>		   
						   </select>
							</div>						 
							 <div class="col-md-5 col-sm-5 padd_0">
							<input type="text" class="form-control" placeholder="Expiration Date">
							</div>
							<div class="col-md-2 col-sm-2">
							<input type="text" class="form-control" placeholder="CVV">
							</div>
						 </div>
					 </div>
					 						
				 
				 
						   
						   <div class="form-group">	
                          <hr>						   
						  <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
					<button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>
							</div>
						   </div>
				 


         
     
	       </form>
         </div>
		 </section>
       <!----===============   billing END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include '../footer-2.php'?>