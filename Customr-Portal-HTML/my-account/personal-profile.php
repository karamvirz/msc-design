<?php include '../header-2.php'?>
	
  <div class="container main-container">
	 <div class="row">
          <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		    <h4 class="sidebar-title">My Account</h4>
				<ul class="nav side-nav"> 
				 <li class="active"><a href="../my-account/personal-profile.php"><i class="ion-ios-person-outline"></i> Personal Profile </a> </li>
				 <li><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings </a> </li> 	 
				  <li><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li>
				  <li><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication </a> </li> 
				  
				  <li class="divider"></li>				  
				  <li><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			      <li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
				   
			 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Personal Profile</h3>
		<form class="pull-right form-inline">
		<a class="btn btn-sm btn-success-outline " href="../my-account/edit-profile.php">
					  <i class="ion-edit"></i> Edit Profile</a></form>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  profile  STARTS here ========--->
		
		<section class="profile">
			<div class="col-md-3 col-sm-3 user-thum text-center"><img src="../images/user-large.png"></div>
			<div class="col-md-9 col-sm-9 user-description">
				<h3>John Smith</h3>
				<p><i class="ion-ios-home"></i> 5716 Corsa Ave, Suite 110, Westlake Village, CA 91362 </p>	
				<p><i class="ion-email"></i> johnSmith@domainname.com</p>		
				<p><i class="ion-android-call"></i> 5806565025</p>		
					
             </div>			 
			 <div class="clearfix"></div>
			<!--div class="col-md-3 col-sm-3 text-right"><h4>Billing Information :</h4></div>
			<div class="col-md-8 col-sm-8">
				 <h5>Card Name here<h5>
				 <p>Billing address here Loream Ipsum Dollar sit</p>
				 <button class="btn btn-primary-outline">Upgrade</button>
			   </div-->
			   
			    <div class="clearfix"></div><hr>
				
				<div class="col-md-3 col-sm-3 text-right padd_right_0">
				<p><strong>About Me :</strong></p></div>
			<div class="col-md-8 col-sm-8">
				 <p>Male</p>
				 <p>Unmarried</p>
			   </div>
			   
			   
			    <div class="clearfix"></div><hr>
				
				<div class="col-md-3 col-sm-3 text-right padd_right_0">
				<p><strong>Annual Household Income :</strong></p></div>
			<div class="col-md-8 col-sm-8"><p>$5000.00</p> </div>
				
				
				
		
			
		</section>
       <!----===============   profile END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

 
<?php include '../footer-2.php'?>