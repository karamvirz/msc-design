<?php include '../header-2.php'?>
	
  <div class="container main-container">
	 <div class="row">
          <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		    <h4 class="sidebar-title">My Account</h4>
				<ul class="nav side-nav"> 
				 <li ><a href="../my-account/personal-profile.php"><i class="ion-ios-person-outline"></i> Personal Profile </a> </li>
				 <li><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings </a> </li> 	 
				  <li class="active"><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li>
				  <li><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication </a> </li> 
				  
				  <li class="divider"></li>				  
				  <li><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			      <li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
				   
			 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Billing Information</h3>
		<form class="pull-right form-inline">
		<a href="edit-billing.php" class="btn btn-sm btn-success-outline">
		<i class="ion-edit"></i> Edit Information</a></form>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  billing  STARTS here ========--->
		
		<section class="billing">
        <div class="col-md-8 col-md-offset-2">		
        <div class="row">		
			<div class="col-md-4 col-sm-4 col-xs-6  padd_right_0 text-right">
				<strong>Name on Card  :</strong></div>				
			<div class="col-md-8 col-sm-8 col-xs-6">John Smith</div>
					
				<div class="clearfix"></div><hr>

			<div class="col-md-4 col-sm-4 col-xs-6  padd_right_0 text-right">
					<strong>Card Number  :</strong></div>				
			<div class="col-md-8 col-sm-8 col-xs-6">
					<p> **** **** **** 4411 </p></div>
					
						<div class="clearfix"></div><hr>
			<div class="col-md-4 col-sm-4 col-xs-6  padd_right_0 text-right">
					<strong>Card Type  :</strong></div>				
			<div class="col-md-8 col-sm-8 col-xs-6">Visa</div>
					
					
						<div class="clearfix"></div><hr>
			<div class="col-md-4 col-sm-4 col-xs-6  padd_right_0 text-right">
					<strong>Expiration Date  :</strong></div>				
			<div class="col-md-8 col-sm-8 col-xs-6">	25 Dec 2016</div>
					
					
					<div class="clearfix"></div><hr>
			<div class="col-md-4 col-sm-4 col-xs-6  padd_right_0 text-right">
					<strong>Security Code  :</strong></div>				
			<div class="col-md-8 col-sm-8 col-xs-6">	***</div>
					
						<div class="clearfix"></div><hr>
			<div class="col-md-4 col-sm-4 col-xs-6  padd_right_0 text-right">
					<strong>Billing Address  :</strong></div>				
			<div class="col-md-8 col-sm-8 col-xs-6">
				5716 Corsa Ave, Suite 110, Westlake Village, CA 91362</div>
		
				
				 
          </div>
         </div>
		 </section>
       <!----===============   profile END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include '../footer-2.php'?>