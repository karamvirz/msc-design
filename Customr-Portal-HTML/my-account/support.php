<?php include '../header-2.php'?>
	
  <div class="container main-container">
	 <div class="row">
          <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		    <h4 class="sidebar-title">My Account</h4>
				<ul class="nav side-nav"> 
				 <li ><a href="../my-account/personal-profile.php"><i class="ion-ios-person-outline"></i> Personal Profile </a> </li>
				 <li><a href="../my-account/security-settings.php"><i class="ion-gear-b"></i> Security Settings </a> </li> 	 
				  <li><a href="../my-account/subscription.php"><i class="ion-clipboard"></i> Billing &amp; Subscription</a></li>
				  <li><a href="../my-account/communication.php"><i class="ion-chatbubble-working"></i> Communication </a> </li> 
				  
				  <li class="divider"></li>				  
				  <li class="active"><a href="../my-account/support.php"><i class="ion-headphone"></i> Support</a></li> 
			      <li><a href="../my-account/faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
				   
			 </ul>  
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Support</h3>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  support  STARTS here ========--->
		
		<section class="support">

        <div class="col-md-10 col-md-offset-1">
		<center><h3>How can we help you?</h3>
		  <p>Loream Ipsum Dollar sit amate is a dummy text</p>
		  </center>
		  <div class="clearfix"></div>
		
		 <div class="support-box col-md-4 col-sm-4">
		 <a href="#"><i class="ion-chatbubble-working"></i> <h4>Live Chat</h4></a>
		    </div>
		 <div class="support-box col-md-4 col-sm-4">
		 <a href="tel:1234560"><i class="ion-android-call"></i> <h4>Phone Support</h4></a>
		    </div>
		 <div class="support-box col-md-4 col-sm-4">
		 <a href="mailto:admin@admin.com"><i class="ion-email"></i> <h4> Contact Support</h4></a>
		    </div>
		 
				 
         </div>
		 </section>
       <!----===============   support END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include '../footer-2.php'?>