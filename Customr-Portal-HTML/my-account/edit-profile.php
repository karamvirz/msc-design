<?php include('../header-2.php')?>	
  <div class="container main-container">
	 <div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">My Account</h4>
				<ul class="nav side-nav"> 
				 <li class="active"><a href="profile.php">
				 <i class="ion-ios-person-outline"></i> Personal Profile </a> </li>
				 <li><a href="security-settings.php"><i class="ion-gear-b"></i> Security Settings </a> </li> 	 
				  <li><a href="billing.php"><i class="ion-clipboard"></i> Billing</a></li>
				  <li><a href="communication.php"><i class="ion-chatbubble-working"></i> Communication </a> </li> 
                  <li class="divider"></li>				  
				  <li><a href="support.php"><i class="ion-headphone"></i> Support</a></li> 
			      <li><a href="faq.php"><i class="ion-help-circled"></i> FAQ</a></li>
				   
			 </ul>
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Edit Personal Profile</h3>
		<form class="pull-right form-inline">
		<a class="btn btn-sm btn-default" href="billing.php">
		<i class="ion-close-circled"></i> Cancel</a></form>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  profile  STARTS here ========--->
		
		<section class="edit-profile">

        <div class="col-md-8 col-md-offset-2">
				 <form class="form-horizontal">
				 
						
				   <div class="form-group">
				   <label>Full Name*</label>
				     <div class="row">						   
				   	   <div class="col-md-4 col-sm-4">
						<input type="text" placeholder="First Name" class="form-control">
						</div>
						 <div class="col-md-4 col-sm-4 padd_0">
						<input type="text" placeholder="Middle Name" class="form-control">
						</div>
						 <div class="col-md-4 col-sm-4">
						<input type="text" placeholder="Last Name" class="form-control">
						</div>
						</div>
						
						</div>
				 
				  <div class="form-group">
				  <hr>
				   <label>Contact Information</label>
				     <div class="row">
                     <!--div class="col-md-6 col-sm-6">
						<input type="text" class="form-control" placeholder="Email*">
						</div-->
									   	  
						</div>						
						</div>
					<div class="clearfix"></div>
					
					 <div class="form-group">
						 <div class="row">
						  <div class="col-md-4 col-sm-4 ">
						<input type="text" class="form-control" placeholder="Phone No.">
						</div><div class="col-md-4 col-sm-4 padd_left_0">
							<input type="text" class="form-control" placeholder="Street">
							</div>
							 <div class="col-md-4 col-sm-4 padd_left_0">
							<input type="text" placeholder="Apartment" class="form-control">
							</div>
						 </div>
					 </div>
					 <div class="form-group">
				     <div class="row">	
						 <div class="col-md-5 col-sm-5">
						<input type="text" placeholder="City" class="form-control">
						</div>
					
						
						 <div class="col-md-5 col-sm-5 padd_0">
							<select class="form-control fancy-select">
							   <option>Select State</option>
							   <option>State</option>
							   <option>State</option>
							   <option> State</option>		   
						   </select>
						</div>
						 <div class="col-md-2 col-sm-2">
						<input type="text" placeholder="Zip" class="form-control">
						</div>
						 
						 </div>
						</div>						
				 

					<div class="form-group text-center"><hr><h4>About You</h4><hr></div>
					
					<div class="form-group">				  
						 <div class="row">	
						  <label class="col-md-5 col-sm-5 col-xs-12 control-label">Gender:</label>		 				   
						   <div class="radio fancy_radio col-md-2 col-sm-2 col-xs-6">
							<label><input type="radio" name="gender" value="male" checked><span>Male</span></label>
						  </div>											   
						   <div class="radio fancy_radio col-md-2 col-sm-2 col-xs-6">
							<label>
							  <input type="radio" name="gender" value="female"><span>Female</span>
							</label>
						  </div>						
							
							</div>						
						</div>
						
						
						<div class="form-group">
                      <div class="row">						
							  <label class="col-md-5 col-sm-5 col-xs-12 control-label">Marital Status:</label>					 				   
							   <div class="col-md-6 col-sm-6 ">
								<select class="form-control fancy-select">
								<option>Single</option>
								<option>Married</option>
								<option>Divorced</option>
								<option>Widowed</option>
								</select>
							  </div>											   
							  </div>											   
								
						</div>
						
						<div class="form-group">
                       <div class="row">						
						<label class="col-md-5 col-sm-5  control-label">Annual Household Income:</label>
						  <div class="col-md-6 col-sm-6 ">
						   <input type="text" placeholder="Anual Income" class="form-control">
						   </div>
						   </div>
						   </div>
						   
						   
						   <div class="form-group">	
                          <hr>						   
						  <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
					<button class="btn btn-primary btn-lg btn-block" type="submit">Save</button>
							</div>
						   </div>
				 


         
     
	       </form>
         </div>
		 </section>
       <!----===============   profile END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include('../footer-2.php')?>	