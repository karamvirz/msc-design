<?php include 'header.php'?>

  <div class="container main-container">
	 <div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">Tools</h4>
				<ul class="nav side-nav"> 				
				 <li class="active">
				 <a href="tools.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li>
				 <li>
				 <a href="tools/credit-score-simulator.php"><i class="ion-ios-timer-outline"></i>Credit Score Simulator </a> </li>
				 <li><a href="tools/home-calculator.php"><i class="ion-ios-home-outline"></i>Home Calculator </a> </li>
				 
				  <li>
				 <a href="tools/debt-calculator.php"><i class="ion-document-text"></i>Debt Calculator</a> </li>
				   <li>
				 <a href="tools/loan-calculator.php"><i class="ion-calculator"></i>Loan Calulator</a> </li>
				 <li>
				 <a href="tools/amortization-calculator.php"><i class="ion-arrow-graph-up-right"></i>Amortization Calculator</a> </li>
				   
			 </ul>
				   
			 </ul>
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Financial Tools</h3>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  support  STARTS here ========--->
		
		<section class="tools">
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
       </p>

        <div class="row">
		
		 <div class="col-md-4 col-sm-4 col-xs-6 padd_right_0">
			 <div class="tool-box">
			 <img src="images/score.png">
			 <h4>Credit Score Simulator</h4>
			 <p>Lorem ipsum dolor sit amet, Dollar sit consectetur adipiscing elit, sed do and eiusmod tempor ....<a href="#">More </a></p>
			 <a href="tools/credit-score-simulator.php" class="btn btn-block btn-primary btn-lg">Try It Now!</a>
			   </div>
		  </div>
		  
		  <div class="col-md-4 col-sm-4 col-xs-6 padd_right_0">
			 <div class="tool-box">
			 <img src="images/home-calculator.png">
			 <h4>Home Affordability Calculator</h4>
			 <p>Lorem ipsum dolor sit amet, Dollar sit consectetur adipiscing elit, sed do and eiusmod tempor ....<a href="#">More </a></p>
			 <a href="" class="btn btn-block btn-primary btn-lg">Try It Now!</a>
			   </div>
		  </div>
		  
		  <div class="col-md-4 col-sm-4 col-xs-6 padd_right_0">
			 <div class="tool-box">
			 <img src="images/repayment-calculator.png">
			 <h4>Debt Repayment Calculator</h4>
			 <p>Lorem ipsum dolor sit amet, Dollar sit consectetur adipiscing elit, sed do and eiusmod tempor ....<a href="#">More </a></p>
			 <a href="" class="btn btn-block btn-primary btn-lg">Try It Now!</a>
			   </div>
		  </div>
		  
		  <div class="col-md-4 col-sm-4 col-xs-6 padd_right_0">
			 <div class="tool-box">
			 <img src="images/simple-calculator.png">
			 <h4>Simple Loan Calculator</h4>
			 <p>Lorem ipsum dolor sit amet, Dollar sit consectetur adipiscing elit, sed do and eiusmod tempor ....<a href="#">More </a></p>
			 <a href="" class="btn btn-block btn-primary btn-lg">Try It Now!</a>
			   </div>
		  </div>
		  
		  <div class="col-md-4 col-sm-4 col-xs-6 padd_right_0">
			 <div class="tool-box">
			 <img src="images/amortization-calculator.png">
			 <h4>Amortization Calculator</h4>
			 <p>Lorem ipsum dolor sit amet, Dollar sit consectetur adipiscing elit, sed do and eiusmod tempor ....<a href="#">More </a></p>
			 <a href="" class="btn btn-block btn-primary btn-lg">Try It Now!</a>
			   </div>
		  </div>
		
		 
				 
         </div>
		 </section>
       <!----===============   support END =======--->
		
			

		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

<?php include('footer.php')?>	