<?php include '../header-2.php'?>

  <div class="container main-container">
	 <div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">Tools</h4>
				<ul class="nav side-nav"> 
				
				 <li> <a href="../tools.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li  class="active">
				 <a href="../tools/credit-score-simulator.php"><i class="ion-ios-timer-outline"></i>Credit Score Simulator </a> </li>
				 <li>
				 <a href="../tools/home-calculator.php"><i class="ion-ios-home-outline"></i>Home Calculator </a> </li>
				 
				  <li>
				 <a href="../tools/debt-calculator.php"><i class="ion-document-text"></i>Debt Calculator</a> </li>
				   <li>
				 <a href="../tools/loan-calculator.php"><i class="ion-calculator"></i>Loan Calulator</a> </li>
				 <li>
				 <a href="../tools/amortization-calculator.php"><i class="ion-arrow-graph-up-right"></i>Amortization Calculator</a> </li>
				   
			 </ul>
				</div>
		<!----====  sidebar END here =====--->
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Credit Score Simulator</h3>
		</div>
		<div class="clearfix"></div>
		
		<!----==========  score-meter  STARTS here ========--->
		
		<section class="score-meter score-simulator-meters">
		<div class="meter col-md-6  col-xs-6">
		    <div class="clearfix"></div>
		    <div class="score-count">760</div>
		    <div class="score-rating">Credit Rating : <strong>Excellent</strong></div>
			 <div class="progress-container">
			  <div style="width: 82%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="82" role="progressbar" class="progress-bar"></div>
			  <div class="clearfix"></div>
			 <div class="progress meter-progress">
				 <span class="poor"></span>
				 <span class="fair"></span>
				 <span class="average"></span>
				 <span class="good"></span>
				 <span class="excellent"></span>
			 </div>
			  <div class="clearfix"></div>
			 <ul class="nav progress-counting">
				 <li>300</li>
				 <li>580</li>
				 <li>640</li>
				 <li>700</li>
				 <li>750</li>
				 <li>850</li>
			  </ul>
			  </div><!---- progress-container-->   
			<p class="updation text-center"><strong>Your Current Score </strong></p>
		
		</div><!---- meter Close here-->
		
		<div class="estimated-score meter col-md-6 col-xs-6">
		    <div class="clearfix"></div>
		    <div class="score-count">810</div>
		    <div class="score-rating">Credit Rating : <strong>Excellent</strong></div>
			 <div class="progress-container">
			  <div style="width: 88%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="88" role="progressbar" class="progress-bar"></div>
			  <div class="clearfix"></div>
			 <div class="progress meter-progress">
				 <span class="poor"></span>
				 <span class="fair"></span>
				 <span class="average"></span>
				 <span class="good"></span>
				 <span class="excellent"></span>
			 </div>
			  <div class="clearfix"></div>
			 <ul class="nav progress-counting">
				 <li>300</li>
				 <li>580</li>
				 <li>640</li>
				 <li>700</li>
				 <li>750</li>
				 <li>850</li>
			  </ul>
			  </div><!---- progress-container-->   
			<p class="updation text-center"><strong>Your Estimated Score </strong></p>
		
		</div><!---- meter Close here-->

		</section>
       <!----===============   score-meter  END =======--->
	   	<div class="clearfix"></div>
		
<!----==========  Score-Simulator  STARTS here ========--->	
       <div class="section-title">
		 <h3>Open or Close a Credit Account</h3>
		</div>	
		<div class="clearfix"></div>
		<section class=" Score-Simulator">       		
		
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/new-loan.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Get a new loan</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a href="#" class="btn btn-primary-outline btn-block">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/new-credit-card.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Open a New Credit Card</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   
		    <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/Transfer-balance.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Transfer balance to New Card</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/close-account.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Close My Oldest Card</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/Application-denied.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Have a Credit Application Denied</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   
		   
		  
	   
		   </section>
<!----==========  Score-Simulator  STARTS here ========--->	


<div class="section-title">
		 <h3>Change How I Use My Cards </h3>
		</div>	
		<div class="clearfix"></div>
			
  <section class=" Score-Simulator">       		
		
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/Credit-Limit-Increase.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Get a Credit Limit Increase</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a href="#" class="btn btn-primary-outline btn-block">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/increase-dec-balance.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Increase/Decrease my Balances</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		
        </section>
		
		
		
		<!----==========  Score-Simulator  STARTS here ========--->	


<div class="section-title">
		 <h3>Change My Payment Habits </h3>
		</div>	
		<div class="clearfix"></div>
			
  <section class=" Score-Simulator">       		
		
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/past-due.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Let My Accounts Go Past Due</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a href="#" class="btn btn-primary-outline btn-block">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/late-payments.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Wipe Out My Late Payment</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		
        </section>
		
		<!----==========  Score-Simulator  STARTS here ========--->	


<div class="section-title">
		 <h3>Get New Nagative Marks</h3>
		</div>	
		<div class="clearfix"></div>
			
  <section class=" Score-Simulator">       		
		
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/pay-tax.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Don't Pay My Taxes</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a href="#" class="btn btn-primary-outline btn-block">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/Foresclosure.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Go Into Foresclosure</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/late-payments.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Have My Wages Garnished</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		   
		   <div class="col-md-6 col-xs-6 padd_right_0">
				<div class="simulate-box">
					 <div class="row">
					 <div class="col-md-2 col-xs-12  padd_right_0">
					  <img src="../images/Account-Sent.png">
					   </div>
					  <div class="col-md-6 col-xs-12 simulate-title">
					  <h4>Have an Account Sent To Collections</h4>
					   </div>
					 <div class="col-md-4 col-xs-12  padd_left_0">
					  <a class="btn btn-primary-outline btn-block" href="#">Simulate Score</a></div>
					</div>
				</div>
		   </div>
		
        </section>

	
		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->

		
		<div class="clearfix"></div>
		
<?php include '../footer-2.php'?>