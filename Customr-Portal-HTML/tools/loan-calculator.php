<?php include '../header-2.php'?>
	
  <div class="container main-container">
	 <div class="row">
         <div class="col-xs-6 col-sm-3 sidebar" id="sidebar">
		   <h4 class="sidebar-title">Tools</h4>
				<ul class="nav side-nav"> 
				
				 <li> <a href="../tools.php"><i class="ion-speedometer"></i> Overview</a> </li>
				 <li >
				 <a href="../tools/credit-score-simulator.php"><i class="ion-ios-timer-outline"></i>Credit Score Simulator </a> </li>
				 <li>
				 <a href="../tools/home-calculator.php"><i class="ion-ios-home-outline"></i>Home Calculator </a> </li>
				 
				  <li>
				 <a href="../tools/debt-calculator.php"><i class="ion-document-text"></i>Debt Calculator</a> </li>
				   <li class="active">
				 <a href="../tools/loan-calculator.php"><i class="ion-calculator"></i>Loan Calulator</a> </li>
				 <li>
				 <a href="../tools/amortization-calculator.php"><i class="ion-arrow-graph-up-right"></i>Amortization Calculator</a> </li>
				   
			 </ul>
				</div>
		<!----====  sidebar END here =====--->
		
		
		<div class="content-container col-sm-9">
		
		<div class="section-title">
		<h3>Simple Loan Calculator</h3>
		</div>
		<div class="clearfix"></div>
	
<!----==========  Home Calculator ========--->	

<section class="Calculators">

       <div class="col-md-8 col-md-offset-2">
           <form class="form-horizontal">				
				   <div class="form-group">
					   <label class="col-md-5 col-sm-5 control-label">Loan Amount :</label>
						 <div class="col-md-5 col-sm-6">
						<div class="input-group"> 
						<div class="input-group-addon">$</div> 
						<input type="text" class="form-control" placeholder="0.00"> 
						</div>
						  </div>
					  </div>
					  
					   <div class="form-group">
					   <label class="col-md-5 col-sm-5 control-label">Numbers of Years :</label>
						 <div class="col-md-5 col-sm-6">
						<div class="input-group"> 
						<div class="input-group-addon"><i class="ion-calendar"></i></div> 
						<input type="text" class="form-control" placeholder="0"> 
						</div>
						  </div>
					  </div>
					
					  
					  
					   <div class="form-group">
					   <label class="col-md-5 col-sm-5 control-label">Interest Rate :</label>
						 <div class="col-md-5 col-sm-6">
						<div class="input-group"> 
						<div class="input-group-addon">%</div> 
						<input type="text" class="form-control" placeholder="0"> 
						</div>
						  </div>
					  </div>
					  
					  
					  <div class="form-group">	
                           <hr>					  
						 <div class="col-md-5 col-sm-9 col-md-offset-3 col-sm-offset-1">	
						  <button class="btn btn-primary btn-lg btn-block" type="submit">CALCULATE</button>
						  </div>
					  </div>
					  
					</form>
                  </div>					

               </section>

<!----==========  Home Calculator ========--->	
    
		<div class="clearfix"></div>

		<div class="section-title">
		<h3>Your Monthly Payments</h3>
		</div>
		<div class="clearfix"></div>
		
		
		<section class="Calculators">
		 <div class="table-responsive">
		<table class="table monthly-payment">
		<thead>
		<tr>
		<th>Loan Amount </th>
		<th>Interest Rates</th>
		<th> Number  of Years</th>
		<th> Monthly Payments</th>	
		</tr>
		</thead>
		<tr>
		<td>-</td>
		<td>-</td>
		<td>-</td>
		<td>-</td>	
		  </tr>

		  </table>
		  </div>
		</section>


		 <div class="clearfix"></div>
		<div class="section-title">
		<h3>Simple Loan Payment Chart</h3>
		</div>
			<div class="clearfix"></div>
			
		<section class="">	  
		   
		   <div class="col-md-5 col-sm-5 pie-chart padd_right_0">
			  <div id="chartContainer"></div>
			 </div> 
			 
			  <div class="col-md-7 col-sm-7 pie-des padd_right_0">
			   <table class="table">
			    <tr><td><span class="blue"></span> Principle</td><td>-</td></tr>				 
				 <tr><td><span class="yellow"></span> Interest</td><td>-</td></tr>			 
				 <tr><td colspan="2">&nbsp;</td></tr>			 
			    </table>
			   </div>
			 
		   
		</section>
		
		

		</div>
<!----===========  content-container END here ======--->	
</div><!----  Row END here --->
 </div><!----  Container END here --->
 
  <script>

window.onload = function () {
	 CanvasJS.addColorSet("greenShades",
                [//colorSet Array

                "#ffbe10",
                "#0193b6"             
                ]);
	var chart = new CanvasJS.Chart("chartContainer",
	{
		//title:{
			//text: " Principal Payment Chart ",
		//},
        animationEnabled: true,
		legend: {
			//verticalAlign: "center",
			//horizontalAlign: "right"
		},
		 colorSet: "greenShades",
		//theme: "theme1",
		data: [
		{        
			type: "pie",
			
			indexLabelFontFamily: "roboto",       
			indexLabelFontSize: 16,
			indexLabelFontWeight: "normal",
			startAngle:0,
			indexLabelFontColor: "#fff",       
			indexLabelLineColor: "#0193b6", 
			indexLabelPlacement: "inside", 
			toolTipContent: "{name}: {y}hrs",
			showInLegend: true,
			indexLabel: "#percent%", 
			dataPoints: [
              //{  y: 52, name: "Time At Work", legendMarkerType: "triangle"},
              {  y: 75, name: "Interest paid", legendMarkerType: "square"},
              {  y: 65, name: "Principal Paid", legendMarkerType: "circle"}
			]
		}
		]
	});
	chart.render();
}

</script>

     
		
		<?php include '../footer-2.php'?>